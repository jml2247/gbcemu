/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef GBSYSTEM_H
#define GBSYSTEM_H

#include <memory>
#include <string>
#include <vector>

// Core
#include "CPU.h"
#include "GPU.h"
#include "Memory.h"

class GBSystem
{
public:
    GBSystem()
        : m_mmu(std::make_unique<Memory>())
        , m_gpu(*m_mmu)
        , m_cpu(*m_mmu, m_gpu) {}

    void runFrame();
    void updateJoypad(u8 padState);
    bool loadCartridge(const std::string &fileName);
    void reset();

    const std::vector<uint16_t> &framebuffer();

private:
    std::unique_ptr<Memory> m_mmu;
    GPU m_gpu;
    CPU m_cpu;

    bool m_paused;
};

#endif
