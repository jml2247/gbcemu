/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "GBSystem.h"

#include "CPU.h"
#include "Cartridge.h"
#include "GPU.h"

void GBSystem::runFrame()
{
    m_cpu.run(70000/4);
}

void GBSystem::updateJoypad(u8 padState)
{
    m_mmu->writeMemory(PAD, padState);
}

bool GBSystem::loadCartridge(const std::string &fileName)
{
    {
        auto c = Cartridge(fileName);
        if (!c.isLoaded()) {
            return false;
        }

        reset();
        m_mmu->loadCartridge(std::move(c));
    }
    
    m_cpu.init();

    return true;
}

void GBSystem::reset()
{
    m_mmu->reset();
    m_cpu.reset();
    m_gpu.reset();
}

const std::vector<uint16_t> &GBSystem::framebuffer()
{
    return m_gpu.framebuffer();
}
