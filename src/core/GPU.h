/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef GPU_H
#define GPU_H

#include "Globals.h"

#include <vector>

class Memory;

class GPU
{
public:
    GPU(const Memory &mmu);

    void reset();
    void updateLine(int y);

    const std::vector<uint16_t> &framebuffer();

private:
    const Memory &m_mmu;

    // Full framebuffer for easy pixmap construction
    std::vector<uint16_t> m_frameBuffer;

    void updateBackground(int y);
    void updateSprites(int y);
    void clearLine(int y);
    void clearFramebuffer();

    Color color(u8 colorNumber, u16 paletteAddress);
};

#endif
