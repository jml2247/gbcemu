/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "Memory.h"

#include "Cartridge.h"
#include "GPU.h"

#include <cstring>

#include <gsl/gsl_assert>

using namespace std;

Memory::Memory()
{
    m_ramPage.resize(0x8000);

    m_memoryMap[0x08] = &m_ramPage[0x0000];
    m_memoryMap[0x09] = &m_ramPage[0x1000];
    m_memoryMap[0x0A] = &m_ramPage[0x2000];
    m_memoryMap[0x0B] = &m_ramPage[0x3000];
    m_memoryMap[0x0C] = &m_ramPage[0x4000];
    m_memoryMap[0x0D] = &m_ramPage[0x5000];
    m_memoryMap[0x0E] = &m_ramPage[0x6000];
    m_memoryMap[0x0F] = &m_ramPage[0x7000];

    init();
}

void Memory::init()
{
    std::fill(m_ramPage.begin(), m_ramPage.end(), 0x00);

    writeRaw(LCDCONT, 0x91);
    writeRaw(LCDSTAT, 0x02);
    writeRaw(SCROLLX, 0x00);
    writeRaw(SCROLLY, 0x00);
    writeRaw(IE     , 0xE0);
    writeRaw(BGP    , 0xFC);
    writeRaw(OBJ0   , 0xFF);
    writeRaw(OBJ1   , 0xFF);
    writeRaw(IF     , 0xE0);
}

void Memory::reset()
{
    init();
}

void Memory::loadCartridge(Cartridge &&c)
{
    Expects(c.isLoaded());
    
    m_cartridge = c;
    m_mbc = makeMemoryBankControllerFor(*m_cartridge);

    mapCartridgeBank0();
    mapCartridgeBankFromController();
}

void Memory::mapCartridgeBank0()
{
    auto zeroBank = m_mbc->zeroBankView();
    
    m_memoryMap[0x00] = &zeroBank[0x0000];
    m_memoryMap[0x01] = &zeroBank[0x1000];
    m_memoryMap[0x02] = &zeroBank[0x2000];
    m_memoryMap[0x03] = &zeroBank[0x3000];
}

void Memory::mapCartridgeBankFromController()
{
    auto bankView = m_mbc->currentBankView();
    
    m_memoryMap[0x04] = &bankView[0x0000];
    m_memoryMap[0x05] = &bankView[0x1000];
    m_memoryMap[0x06] = &bankView[0x2000];
    m_memoryMap[0x07] = &bankView[0x3000];
}

uint8_t Memory::readMemory(uint16_t address) const
{
    return m_memoryMap[address >> 12][address & 0x0FFF];
}

void Memory::writeMemory(uint16_t address, uint8_t data)
{
    if (address < 0x8000) {
        m_mbc->write(address, data);
        mapCartridgeBankFromController();
        return;
    }
    
    if (address == 0xFF80) return; // FIXME: Tetris hack

    writeRaw(address, data);

    // Mirror the range 0xC000 - 0xDDFF
    if (address >= 0xC000 && address < 0xDE00) {
        address += 0x2000;
        writeRaw(address, data);
        return;
    } else if ((address >= 0xE000) && (address < 0xFE00)) {
        address -= 0x2000;
        writeRaw(address, data);
        return;
    }

    switch (address) {
    case DMA:
        for (unsigned int i = 0; i < 0xA0; ++i) {
            // E.g. 9A becomes 9A00
            auto value = readMemory((data << 8) + i);
            uint16_t dest = 0xFE00 + i;
            writeRaw(dest, value);
        }
        break;
    case LCDSTAT:
        writeRaw(LCDSTAT, (data & 0xF8) | (readMemory(LCDSTAT) & 0x07));
        break;
    case LY:
        writeRaw(LY, 0);
        break;
    case DIV:
        writeRaw(DIV, 0);
        break;
    }
}

void Memory::writeRaw(uint16_t address, uint8_t data)
{
    m_memoryMap[address >> 12][address & 0x0FFF] = data;
}
