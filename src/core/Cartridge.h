/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef CARTRIDGE_H
#define CARTRIDGE_H

#include "Globals.h"

#include <string>
#include <vector>

enum class CartridgeType : uint8_t
{
    ROMOnly = 0x0,
    MBC1,
    MBC1AndRAM,
    MBC1AndRAMAndBattery,
    MBC2,
    MBC2AndBattery,
    ROMAndRAM,
    ROMAndRAMAndBattery,
    MMM01,
    MMM01AndRAM,
    MMM01AndRAMAndBattery,
    MBC3AndTimerAndBattery,
    MBC3AndTimerAndRAMAndBattery,
    MBC3,
    MBC3AndRAM,
    MBC3AndRAMAndBattery,
    MBC5,
    MBC5AndRAM,
    MBC5AndRAMAndBattery,
    MBC5AndRumble,
    MBC5AndRumbleAndRAM,
    MBC5AndRumbleAndRAMAndBattery,
    MBC6,
    MBC7AndSensorAndRumbleAndRAMAndBattery,
    PocketCamera = 0xFC,
    BandaiTAMA5,
    HuC3,
    HuC3AndRAMAndBattery
};

enum class CartridgeSize : uint8_t
{
    KByte32 = 0x0,
    KByte64,
    KByte128,
    KByte256,
    KByte512,
    MByte1,
    MByte2,
    MByte4,
    MByte8,
    MByte1_1 = 0x52,
    MByte1_2,
    MByte1_4
};

class Cartridge
{
public:
    Cartridge(const std::string &fileName);

    bool isLoaded();
    std::vector<uint8_t> &data();
    
    CartridgeType type() const;
    std::string typeString() const;
    
    CartridgeSize sizeType() const;
    size_t size() const;
    
    constexpr static uint16_t CartridgeTypeAddress = 0x0147;
    constexpr static uint16_t CartridgeSizeAddress = 0x0148;

private:
    bool m_isLoaded;
    std::vector<uint8_t> m_cartridge; // 0x0000 - 0x3FFF, 0x4000 - 0x7FFF ROM (bank 0, banks 1-n)
};

#endif
