/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef MBC_H
#define MBC_H

#include <iostream>
#include <memory>

#define GSL_TERMINATE_ON_CONTRACT_VIOLATION
#include <gsl/span>

#include "Globals.h"
#include "Cartridge.h"

class MemoryBankController
{
public:
    constexpr static u16 MemoryBankSize = 0x4000;

    MemoryBankController(Cartridge &cart)
      : m_cartridge(cart) {
    }
    
    virtual ~MemoryBankController() = default;
    
    gsl::span<u8, MemoryBankSize> zeroBankView() {
        return gsl::make_span(m_cartridge.data().data(), static_cast<ptrdiff_t>(MemoryBankSize));
    }

    virtual gsl::span<u8, MemoryBankSize> currentBankView() const = 0;
    virtual void write(u16 address, u8 data) = 0;
    
protected:
    Cartridge &m_cartridge;
};

class RomOnlyMBC : public MemoryBankController
{
public:
    RomOnlyMBC(Cartridge &cart)
        : MemoryBankController(cart)
        , m_romView(gsl::make_span(cart.data())) {
    }
    
    gsl::span<u8, MemoryBankSize> currentBankView() const override {
        return m_romView.subspan(0x4000, MemoryBankSize);
    };
    
    void write(u16, u8) override {
        std::cout << "Warning! Writing to control registers of a ROM-only cartridge\n";
    }

private:
    gsl::span<u8, -1> m_romView;
};

class MBC1 : public MemoryBankController
{
public:
    MBC1(Cartridge &cart)
        : MemoryBankController(cart)
        , m_romView(m_cartridge.data())
        , m_currentBank(1) {
    }
    
    gsl::span<u8, MemoryBankSize> currentBankView() const override {
        return m_romView.subspan(m_currentBank * MemoryBankSize, MemoryBankSize);
    }
    
    void write(u16 address, u8 data) override {
    }

private:
    gsl::span<u8, -1> m_romView;
    u16 m_currentBank;
};

inline std::unique_ptr<MemoryBankController> makeMemoryBankControllerFor(Cartridge &cartridge)
{
    switch (cartridge.type()) {
    case CartridgeType::ROMOnly:
        return std::make_unique<RomOnlyMBC>(cartridge);
    case CartridgeType::MBC1:
        return std::make_unique<MBC1>(cartridge);
    default:
        break;
    }
    
    return nullptr;
}

#endif
