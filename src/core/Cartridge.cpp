/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "Cartridge.h"

// std includes
#include <fstream>
#include <iostream>

using namespace std;

constexpr int MaxCartSize = 2097152;

constexpr const char* cartridgeTypeToString(CartridgeType type);
constexpr size_t cartridgeSizeToBytes(CartridgeSize size);

Cartridge::Cartridge(const std::string &fileName)
    : m_isLoaded(false)
{
    ifstream file{fileName.c_str(), ios::in|ios::binary|ios::ate};

    if (!file.is_open())
        return;

    auto fileSize = static_cast<size_t>(file.tellg());
    if (fileSize > MaxCartSize) {
        cerr << fileName << ": WARNING, cartridge larger than 2MB. Probably not a valid ROM\n";
        fileSize = MaxCartSize;
    }

    // Resize the cartridge vector to hold the filesize, setting a floor of 32KB for carts that lie about their size
    m_cartridge.resize(std::max(fileSize, static_cast<size_t>(32_KB)));

    file.seekg(0, ios::beg);
    file.read((char *)m_cartridge.data(), fileSize);
    file.close();
    
    auto cartSizeHeader = size();
    
    if (fileSize != cartSizeHeader) {
        cout << "Cartridge claims to be " << cartSizeHeader << " bytes, but ROM file is only " << fileSize << " bytes\n";
    }

    cout << fileName << ":\nFile loaded in memory correctly, cartridge type: " << typeString() << '\n';
    m_isLoaded = true;
}

bool Cartridge::isLoaded()
{
    return m_isLoaded;
}

std::vector<uint8_t> &Cartridge::data()
{
    return m_cartridge;
}

CartridgeType Cartridge::type() const
{
    return static_cast<CartridgeType>(m_cartridge.at(CartridgeTypeAddress));
}

std::string Cartridge::typeString() const
{
    return cartridgeTypeToString(type());
}

constexpr const char* cartridgeTypeToString(CartridgeType type)
{
    switch(type)
    {
    case CartridgeType::ROMOnly:
        return "ROM Only";
    case CartridgeType::MBC1:
        return "MBC1";
    default:
        return "Invalid cartridge type";
    }
}

CartridgeSize Cartridge::sizeType() const
{
    return static_cast<CartridgeSize>(m_cartridge.at(CartridgeSizeAddress));
}

size_t Cartridge::size() const
{
    return cartridgeSizeToBytes(sizeType());
}

constexpr size_t cartridgeSizeToBytes(CartridgeSize size)
{
    switch(size)
    {
    case CartridgeSize::KByte32:
        return 32_KB;
    case CartridgeSize::KByte64:
        return 64_KB;
    case CartridgeSize::KByte128:
        return 128_KB;
    case CartridgeSize::KByte256:
        return 256_KB;
    case CartridgeSize::KByte512:
        return 512_KB;
    case CartridgeSize::MByte1:
        return 1_MB;
    case CartridgeSize::MByte2:
        return 2_MB;
    case CartridgeSize::MByte4:
        return 4_MB;
    case CartridgeSize::MByte8:
        return 8_MB;
    case CartridgeSize::MByte1_1:
        return 1.1e6;
    case CartridgeSize::MByte1_2:
        return 1.2e6;
    case CartridgeSize::MByte1_4:
        return 1.4e6;
    default:
        return 0;
    }
}

