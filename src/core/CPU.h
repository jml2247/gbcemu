/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

// Custom Nintendo Z80 CPU emulator

#ifndef CPU_H
#define CPU_H

#include "Globals.h"

class GPU;
class Memory;

// Condition flags
#define FLAG_Z 0x80
#define FLAG_N 0x40
#define FLAG_H 0x20
#define FLAG_C 0x10

class CPU
{
public:
    CPU(Memory &memory, GPU &gpu);

    void setGPU(GPU *gpu);
    void init();
    void reset();

   /**
    * Runs for the given cycles (until an interrupt is expected)
    */
    void run(int cyclesToRun);

private:
    // Program Counter
    GBRegister m_PC;
    // Stack Pointer
    GBRegister m_SP;
    // A/F combined register
    GBRegister m_AF;
    // B/C combined register
    GBRegister m_BC;
    // D/E combined register
    GBRegister m_DE;
    // H/L combined register
    GBRegister m_HL;
    // Interrupt pending flag
    uint16_t m_IFF;
    bool m_setIFF;
    // Halt flag
    bool m_halt; // FIXME: hack. Supporting hardware-accurate halt is more complicated

    // Used for GPU timing
    int m_cyclesLCD;
    int m_cyclesTimer;
    int m_cyclesDivider;

    Memory &m_memory;
    GPU &m_gpu;

    // Interrupt and Timer emulation
    void handleInterrupts();
    void updateLCDState();
    void setNewLCDVideoMode(VideoMode mode);
    void updateTimers();
    void requestInterrupt(InterruptFlag flag);
   /**
    * Checks for LY register coincidence, and handles if there is one
    */
    void checkLYC();
};

#endif
