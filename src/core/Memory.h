/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

// A class emulating the Gameboy MMU

#ifndef MEMORY_H
#define MEMORY_H

#include <array>
#include <optional>
#include <vector>
#include <cstdint>

#include "Globals.h"
#include "Cartridge.h"
#include "MBC.h"

class MemoryBankController;

class Memory
{
public:
    Memory();
    ~Memory() = default;

    void init();
    void reset();
    void loadCartridge(Cartridge &&c);

    uint8_t readMemory(uint16_t address) const;
    void writeMemory(uint16_t address, uint8_t data);

    // Used for internal ram writes, no checks
    void writeRaw(uint16_t address, uint8_t data);

private:
    std::array<uint8_t *, 0x10> m_memoryMap;
    std::optional<Cartridge> m_cartridge;
    std::unique_ptr<MemoryBankController> m_mbc;
    std::vector<uint8_t> m_ramPage; // 0x8000 - 0x9FFF, Video RAM
                                    // 0xA000 - 0xBFFF, External Cartridge RAM
                                    // 0xC000 - 0xFFFF, Working RAM, shadow, I/O, HiMem
    void mapCartridgeBank0();
    void mapCartridgeBankFromController();
};

#endif
