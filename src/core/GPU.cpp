/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *   Drawign algorithms modeled from CodeSlinger's GB emulator             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "GPU.h"

#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <iostream>

#include <gsl/gsl_util>

#include "Memory.h"

uint16_t colorToFrameBufferValue(Color col) noexcept;

GPU::GPU(const Memory &mmu)
   : m_mmu(mmu)
   , m_frameBuffer(GB_SCREEN_W * GB_SCREEN_H)
{
    // Fill framebuffer with black initially
    clearFramebuffer();
}

void GPU::reset()
{
    clearFramebuffer();
}

void GPU::updateLine(int y)
{
    uint8_t lcdcont = m_mmu.readMemory(LCDCONT);
    bool lcdEnabled = BIT7(lcdcont);
    
    if (y > GB_SCREEN_H) {
        std::cout << "Warning! Trying to draw line outside of GB screen's height!\n";
        return;
    }

    if (!lcdEnabled) {
        return;
    }

    updateBackground(y);
    updateSprites(y);
}

void GPU::updateBackground(int y)
{
    uint8_t lcdcont = m_mmu.readMemory(LCDCONT);
    bool bgEnabled = BIT0(lcdcont);
    if (!bgEnabled) {
        clearLine(y);
        return; // LCD enabled, but background is off
    }

    uint16_t tileAddress;
    uint16_t tableAddress;

    uint8_t scrollY = m_mmu.readMemory(SCROLLY);
    uint8_t scrollX = m_mmu.readMemory(SCROLLX);
    uint8_t windowY = m_mmu.readMemory(WNDY);
    uint8_t windowX = m_mmu.readMemory(WNDX) - 7;

    // We are only using a window if the window is enabled and if the
    // scanline we are drawing is inside the window
    bool usingWindow = BIT5(lcdcont) && windowY <= m_mmu.readMemory(LY);

    // Where are we getting tiles from?
    // The sprite table at 0x8000 uses unsigned indexing, the table at 0x8800 uses signed
    tileAddress = BIT4(lcdcont) ? 0x8000 : 0x8800;
    bool unsignedTiles = BIT4(lcdcont);

    if (!usingWindow) {
        // Which background tile table to use?
        tableAddress = BIT3(lcdcont) ? 0x9C00 : 0x9800;
    } else {
        // Which window tile table to use?
        tableAddress = BIT6(lcdcont) ? 0x9C00 : 0x9800;
    }

    uint8_t yPosRaw = m_mmu.readMemory(LY);
    uint8_t yPos = usingWindow ? yPosRaw - windowY: yPosRaw + scrollY;
    uint16_t tileRow = ((uint8_t)(yPos/8))*32;

    for (uint8_t pixel = 0; pixel < 160; ++pixel) {
        uint8_t xPos = pixel + scrollX;

        if (usingWindow && (pixel >= windowX)) {
            xPos = pixel - windowX;
        }

        uint16_t tileCol = xPos/8;
        int16_t tileNum;
        const uint16_t tileNumLocation = tableAddress + tileRow + tileCol;

        if (unsignedTiles) {
            tileNum = m_mmu.readMemory(tileNumLocation);
        } else {
            tileNum = (int16_t)m_mmu.readMemory(tileNumLocation);
        }

        uint16_t tileLocation = tileAddress;

        unsignedTiles ? tileLocation += (tileNum * 16) :
                        tileLocation += ((tileNum + 128) * 16);

        uint8_t line = (yPos % 8) * 2; // 2 bytes per pixel

        // Pixel data is kept in two bytes, one on top of the other, when
        // viewed as a memory map that's one byte wide.
        uint8_t pixelData1 = m_mmu.readMemory(tileLocation + line);
        uint8_t pixelData2 = m_mmu.readMemory(tileLocation + line + 1);

        int colorBit = (xPos % 8) - 7;
        colorBit *= -1;

        uint8_t mask = 1 << colorBit;
        uint8_t colorNumber = (pixelData2 & mask) != 0 ? 1 : 0;
        // Shift left 1 to make room for first color bit
        colorNumber <<= 1;
        colorNumber |= (pixelData1 & mask) ? 1 : 0;

        Color col = color(colorNumber, BGP);
        uint16_t fbColor = colorToFrameBufferValue(col);

        if (yPosRaw > 143 || pixel > 159) {
            std::cout << "pixel out of range: " << pixel << "\n";
            std::cout << "Or perhaps vblank? " << yPosRaw << "\n";
            continue;
        }
        
        size_t framebufferOffset = (yPosRaw * GB_SCREEN_W) + pixel;

        gsl::at(m_frameBuffer, framebufferOffset) = fbColor;
    }
}

void GPU::updateSprites(int y)
{
    auto lcdcont = m_mmu.readMemory(LCDCONT);
    if (!BIT1(lcdcont)) {
        return;
    }

    // Check if we're using 8x8 or 8x16 sprites
    bool usingLargeSprites = BIT2(lcdcont) != 0;
    int spriteYSize = usingLargeSprites ? 16 : 8;

    for (int8_t spriteS = 39; spriteS >= 0; --spriteS) {
        auto sprite = static_cast<uint8_t>(spriteS);
        uint16_t lineAddress = 0xFE00 + (sprite * 4); // Each sprite attribute entry is 4 bytes
        
        int8_t yPos = m_mmu.readMemory(lineAddress) - 16;
        int8_t xPos = m_mmu.readMemory(lineAddress + 1) - 8;
        uint8_t tileLocation = m_mmu.readMemory(lineAddress + 2);
        
        if (usingLargeSprites) {
            tileLocation &= 0xFE;
        } else {
            tileLocation |= 0x1;
        }

        uint8_t attributes = m_mmu.readMemory(lineAddress + 3);

        int scanline = y;
        if ((scanline >= yPos) && (scanline < (yPos+spriteYSize))) {
            int line = scanline - yPos;
            bool yFlip = BIT6(attributes);
            bool xFlip = BIT5(attributes);

            if (yFlip) {
                line -= spriteYSize;
                line *= -1 ;
            }

            line *= 2;

            uint16_t pixelDataAddress = 0x8000 + (tileLocation * 16) + line;
            uint8_t pixelData1 = m_mmu.readMemory(pixelDataAddress);
            uint8_t pixelData2 = m_mmu.readMemory(pixelDataAddress + 1);

            for (int tilePixel = 7; tilePixel >= 0; tilePixel--) {
                int colorBit = tilePixel;
                if (xFlip){
                    colorBit -= 7;
                    colorBit *= -1;
                }

                uint8_t mask = 1 << colorBit;
                uint8_t colorNumber = (pixelData2 & mask) ? 1 : 0;
                // Shift left 1 to make room for first color bit
                colorNumber <<= 1;
                colorNumber |= (pixelData1 & mask) ? 1 : 0;

                uint16_t paletteAddress = BIT4(attributes) ? OBJ1 : OBJ0;
                Color col = color(colorNumber, paletteAddress);

                // white is transparent for sprites.
                if (col == Color::White) {
                    continue;
                }

                uint16_t fbColor = colorToFrameBufferValue(col);

                int xPix = 0 - tilePixel + 7;
                int pixel = xPos+xPix;

                if (scanline < 0 || scanline > 143 || pixel < 0 || pixel > 159)
                    continue;

                // Check if this pixel is actually on-screen, sprites can be offscreen partially
                size_t bufferOffset = (scanline * GB_SCREEN_W) + pixel;

                gsl::at(m_frameBuffer, bufferOffset) = fbColor;
            }
        }
    }
}

void GPU::clearLine(int y)
{
    auto begin = m_frameBuffer.begin() + (y * GB_SCREEN_W);

    std::fill_n(begin, GB_SCREEN_W, 0xFFFF);
}

void GPU::clearFramebuffer()
{
    std::fill(m_frameBuffer.begin(), m_frameBuffer.end(), 0xFFFF);
}

Color GPU::color(uint8_t colorNumber, uint16_t paletteAddress)
{
    auto res = static_cast<uint8_t>(Color::White);
    uint8_t hi = 0;
    uint8_t lo = 0;

    uint8_t palette = m_mmu.readMemory(paletteAddress);

    switch (colorNumber) {
    case 0: hi = 1; lo = 0; break ;
    case 1: hi = 3; lo = 2; break ;
    case 2: hi = 5; lo = 4; break ;
    case 3: hi = 7; lo = 6; break ;
    default: break;
    }

    uint8_t hiMask = 1 << hi;
    uint8_t loMask = 1 << lo;
    res = ((palette & hiMask) ? 1 : 0) << 1;
    res |= (palette & loMask) ? 1 : 0;

    return static_cast<Color>(res);
}

uint16_t colorToFrameBufferValue(Color col) noexcept
{
    uint16_t color;

    switch (col) {
    case Color::White:
        color = 0xFFFF;
        break;
    case Color::LightGrey:
        color = 0xCCCC;
        break;
    case Color::DarkGrey:
        color = 0x7777;
        break;
    case Color::Black:
    default:
        color = 0x0000;
        break;
    }
    
    return color;
}

const std::vector<uint16_t> &GPU::framebuffer()
{
    return m_frameBuffer;
}
