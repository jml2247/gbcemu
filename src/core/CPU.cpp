/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "CPU.h"

#include <array>
#include <cassert>
#include <cstdio>
#include <gsl/gsl_util>
#include <iostream>

#include "GPU.h"
#include "Memory.h"
#include "Tables.h"

CPU::CPU(Memory &memory, GPU &gpu)
    : m_halt(false)
    , m_cyclesLCD(0)
    , m_cyclesTimer(0)
    , m_cyclesDivider(0)
    , m_memory(memory)
    , m_gpu(gpu)
{
}

void CPU::init()
{
    reset();
}

void CPU::reset()
{
    // Load default register values
    m_SP.W = 0xFFFE;
    m_AF.W = 0x01B0;
    m_BC.W = 0x0013;
    m_DE.W = 0x00D8;
    m_HL.W = 0x014D;
    m_PC.W = 0x0100;
    m_IFF = 0;
    m_setIFF = false;
    m_halt = false;
    m_cyclesLCD = 0;
    m_cyclesTimer = 0;
    m_cyclesDivider = 0;
}

void CPU::run(int cyclesToRun)
{
    uint8_t opcode1 = 0;
    uint8_t opcode2 = 0;
    int cyclesSpent = 0;

    // Used by opcode emulation implementation
    auto tempRegister = GBRegister{};
    u8 tempValue;
    int16_t offset;

    while (true) {
        handleInterrupts();

        if (m_setIFF) {
            m_IFF = 1;
            m_setIFF = false;
        }

        if (m_halt) {
            // HALT state causes execution of 4-cycle NOP until Interrupt
            cyclesSpent = 4;
        } else {
            opcode1 = m_memory.readMemory(m_PC.W++);
            //fprintf(stdout, "opcode: %02x\n", opcode1);
            //fprintf(stdout, "PC: %04x\n", m_PC.W);

            // Check if we're using extended opcodes or not
            if (opcode1 == 0xCB) {
                opcode2 = m_memory.readMemory(m_PC.W++);
                cyclesSpent = gsl::at(cyclesCB, opcode2);
            } else {
                cyclesSpent = gsl::at(cycles, opcode1);
            }

            // Run opcodes
            switch (opcode1) {
            case 0xCB:
                // Run extended
#include "OpcodesCB.h"
                break;
            default:
                // Run regular
#include "Opcodes.h"
                break;
            }
        }

        cyclesToRun -= cyclesSpent;
        m_cyclesLCD += (cyclesSpent * 4);
        m_cyclesDivider += (cyclesSpent *4);
        m_cyclesTimer += (cyclesSpent *4);

        updateLCDState();
        updateTimers();

        if (cyclesToRun < 1) {
            return;
        }
    }
}

void CPU::handleInterrupts()
{
    u8 interrupts = m_memory.readMemory(IF) & m_memory.readMemory(IE) & 0x1F;

    if (interrupts == 0) {
        // Nothing was enabled/set
        return;
    }

    m_halt = false; // Reset halt

    // Do nothing if the Interrupt Master Enable is off
    if (m_IFF == 0) {
        return;
    }

    m_IFF = 0; // Disable further interrupts

    // Push PC to stack
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);

    if (BIT0(interrupts)) {
        // VBlank
        std::cout << "Vblank interrupt\n";
        m_memory.writeMemory(IF, (m_memory.readMemory(IF) & ~0x01));
        m_PC.W = 0x40;
    } else if (BIT1(interrupts)) {
        // LCD Status
        std::cout << "lcstatus\n";
        m_memory.writeMemory(IF, (m_memory.readMemory(IF) & ~0x02));
        m_PC.W = 0x48;
    } else if (BIT2(interrupts)) {
        // Timer
        std::cout << "Timer interrupt\n";
        m_memory.writeMemory(IF, (m_memory.readMemory(IF) & ~0x04));
        m_PC.W = 0x50;
    } else if (BIT3(interrupts)) {
        // Serial
        std::cout << "Serial interrupt\n";
        m_memory.writeMemory(IF, (m_memory.readMemory(IF) & ~0x08));
        m_PC.W = 0x58;
    } else if (BIT4(interrupts)) {
        // Joypad
        std::cout << "joypad interrupt\n";
        m_memory.writeMemory(IF, (m_memory.readMemory(IF) & ~0x10));
        m_PC.W = 0x60;
    }

    m_IFF = 1;
}

void CPU::updateLCDState()
{
    const auto screenEnabled = static_cast<bool>(BIT7(m_memory.readMemory(LCDCONT)));

    const auto lcdstat = m_memory.readMemory(LCDSTAT);
    const auto mode = static_cast<VideoMode>(BITS01(lcdstat));
    const auto hblankInterruptRequested = static_cast<bool>(BIT3(lcdstat));
    const auto vblankInterruptRequested = static_cast<bool>(BIT4(lcdstat));
    const auto oamInterruptRequsted = static_cast<bool>(BIT5(lcdstat));
    const auto ly = m_memory.readMemory(LY);

    switch (mode) {
    case VideoMode::HBlank: // H-Blank
        if (m_cyclesLCD < CyclesPerHBlank) {
            break;
        }
        
        if (ly == 144) { // Time for V-Blank
            // Set LCD status register's mode to mode 1 (VBlank)
            setNewLCDVideoMode(VideoMode::VBlank);
            
            if (!screenEnabled) {
                // Request V-Blank
                requestInterrupt(InterruptFlag::VBlank);
                
                // If needed, request LCD Status Interrupt for mode change to V-Blank
                if (vblankInterruptRequested) {
                    requestInterrupt(InterruptFlag::LCDStat);
                }
            }
            
            m_cyclesLCD -= CyclesPerVBlank;
        } else { // Continue with H-Blank business as usual
            setNewLCDVideoMode(VideoMode::AccessOAM);
            
            // If needed, request LCD Status Interrupt for mode change to OAM
            if (oamInterruptRequsted) {
                requestInterrupt(InterruptFlag::LCDStat);
            }

            m_gpu.updateLine(ly);
            m_cyclesLCD -= CyclesPerOAMAccess;
        }
        
        // Done with this line, increment
        m_memory.writeRaw(LY, ly + 1);
        checkLYC();
        break;
    case VideoMode::VBlank: // V-Blank
        if (m_cyclesLCD < CyclesPerVBlank) {
            break;
        }
        
        if (ly == 153) { // End of V-Blank (range 144 - 153)
            m_memory.writeRaw(LY, 0);

            // Set LCD status register's mode to LCD mode 2 (OAM accessed by LCD)
            setNewLCDVideoMode(VideoMode::AccessOAM);

            // If needed, request LCD Status Interrupt for mode change to OAM
            if (oamInterruptRequsted) {
                requestInterrupt(InterruptFlag::LCDStat);
            }
        } else {
            // Done with this line, increment
            m_memory.writeRaw(LY, ly + 1);
            checkLYC();
            m_cyclesLCD -= CyclesPerVBlank;
            }
        break;
    case VideoMode::AccessOAM: // OAM accessed by LCD
        if (m_cyclesLCD < CyclesPerOAMAccess) {
            break;
        }
        
        // Advance to mode 3 (Access VRAM)
        setNewLCDVideoMode(VideoMode::AccessVRAM);
        m_cyclesLCD -= CyclesPerVRAMAccess;
        break;
    case VideoMode::AccessVRAM: // VRAM accessed by LCD
        if (m_cyclesLCD < CyclesPerVRAMAccess) {
            break;
        }

        // Set video mode to HBlank
        setNewLCDVideoMode(VideoMode::HBlank);
        
        // TODO: Do DMA unless HALT

        // If there's an H-Blank Interrupt pending, request an LCD Status interrupt so we can handle it
        if (hblankInterruptRequested && screenEnabled) {
            requestInterrupt(InterruptFlag::LCDStat);
        }

        m_cyclesLCD -= CyclesPerHBlank;
        break;
    }
}

void CPU::setNewLCDVideoMode(VideoMode mode)
{
    auto newLcdStat = m_memory.readMemory(LCDSTAT);
    
    newLcdStat &= ~0x03; // Clear video mode
    newLcdStat |= static_cast<uint8_t>(mode); // Set new mode
    
    m_memory.writeRaw(LCDSTAT, newLcdStat);
}

void CPU::requestInterrupt(InterruptFlag flag)
{
    auto flags = m_memory.readMemory(IF);
    flags |= static_cast<uint8_t>(flag);
    
    m_memory.writeMemory(IF, flags);
}

// Overflow thresholds for various clock speeds
constexpr std::array<uint16_t, 4> overflowTimer { { 1024, 16, 64, 256 } };

void CPU::updateTimers()
{
    // If the timer is enabled
    if (BIT2(m_memory.readMemory(TIMCONT))) {
        
        // Check for overflow
        auto overflowTimerIndex = BITS01(m_memory.readMemory(TIMCONT));
        if (m_cyclesTimer >= gsl::at(overflowTimer, overflowTimerIndex)) {
            // Handle overflow
            if (m_memory.readMemory(TIMMOD) == 0xFF) {
                m_memory.writeMemory(TIMCNT, m_memory.readMemory(TIMMOD));
            } else {
                m_memory.writeMemory(TIMCNT, m_memory.readMemory(TIMCNT) +1);
            }

            m_cyclesTimer = 0;
        }
    } else {
        m_cyclesTimer = 0;
    }

    if (m_cyclesDivider >= 256) {
        m_memory.writeRaw(DIV, m_memory.readMemory(DIV) + 1);
        m_cyclesDivider = 0;
    }
}

void CPU::checkLYC()
{
    // Check for coincidence
    if (m_memory.readMemory(LY) == m_memory.readMemory(LYC)) {
        m_memory.writeRaw(LCDSTAT, m_memory.readMemory(LCDSTAT) | 0x04);
        // If needed, request an LCD status interrupt
        if (BIT6(m_memory.readMemory(LCDSTAT))) {
            requestInterrupt(InterruptFlag::LCDStat);
        }
    } else {
        // No match
        m_memory.writeRaw(LCDSTAT, m_memory.readMemory(LCDSTAT) & ~0x04);
    }
}

