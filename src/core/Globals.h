/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *   Copyright (C) 2011 by Pablo Gasco                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

// Global typdefs and the like

#ifndef GLOBALS_H
#define GLOBALS_H

#include <cstdint>

typedef signed char s8;

typedef unsigned char u8;
typedef unsigned short u16;

constexpr auto operator""_KB( unsigned long long const x )
    -> long
{ return 1024L*x; }

constexpr auto operator""_MB( unsigned long long const x )
    -> long
{ return 1024L*1024L*x; }

// 16-bit register
typedef union {
  struct {
#ifdef __BIG_ENDIAN__
    uint8_t B1, B0;
#else
    uint8_t B0, B1;
#endif
  } B;
  uint16_t W;
} GBRegister;

enum class Color : uint8_t {
    White = 0,
    LightGrey = 1,
    DarkGrey = 2,
    Black = 3
};

enum class GBKey : uint8_t {
    KeyRight = 0,
    KeyLeft = 1,
    KeyUp = 2,
    KeyDown = 3,
    KeyA = 4,
    KeyB = 5,
    KeySelect = 6,
    KeyStart = 7
};

enum class VideoMode : uint8_t
{
    HBlank = 0,
    VBlank,
    AccessOAM,
    AccessVRAM
};

enum class InterruptFlag : uint8_t
{
    VBlank = 1,
    LCDStat = 2,
    Timer = 4,
    Serial = 8,
    Joypad = 16
};

#define BIT0(value)     ((value) & 0x01)
#define BIT1(value)     ((value) & 0x02)
#define BIT2(value)     ((value) & 0x04)
#define BIT3(value)     ((value) & 0x08)
#define BIT4(value)     ((value) & 0x10)
#define BIT5(value)     ((value) & 0x20)
#define BIT6(value)     ((value) & 0x40)
#define BIT7(value)     ((value) & 0x80)

#define BITS01(value)   ((value) & 0x03)
#define BITS23(value)   ((value) & 0x0C)
#define BITS45(value)   ((value) & 0x30)
#define BITS67(value)   ((value) & 0xC0)

#define GB_SCREEN_W 160
#define GB_SCREEN_H 144
constexpr int CyclesPerHBlank = 204;     // 204-207
constexpr int CyclesPerVBlank = 456;     // 456 * 10 = 4560
constexpr int CyclesPerOAMAccess = 80;   // 77-83
constexpr int CyclesPerVRAMAccess = 172; // 169-175

constexpr uint16_t PAD = 0xFF00;
#define DIV        0xFF04
#define TIMCNT     0xFF05
#define TIMMOD     0xFF06
#define TIMCONT    0xFF07
#define LCDCONT    0xFF40
#define LCDSTAT    0xFF41
#define SCROLLY    0xFF42
#define SCROLLX    0xFF43
#define LY         0xFF44
#define LYC        0xFF45
#define DMA        0xFF46
#define BGP        0xFF47
#define OBJ0       0xFF48
#define OBJ1       0xFF49
#define WNDY       0xFF4A
#define WNDX       0xFF4B
#define IF         0xFF0F
#define IE         0xFFFF

#endif
