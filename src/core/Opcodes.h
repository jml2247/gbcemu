/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

// Only intended to be included from CPU::run(). This switch is in a separate
// file to help maintain readability in the CPU class

#ifndef OPCODES_H
#define OPCODES_H

// Condition flag #defines located in CPU.h

switch (opcode1) {
case 0x00:
    // NOP
    break;
case 0x01:
    // LD BC<word>
    // Loads the next word into the B/C register pair
    m_BC.B.B0 = m_memory.readMemory(m_PC.W++);
    m_BC.B.B1 = m_memory.readMemory(m_PC.W++);
    if (m_BC.W == 0x1200) {
        std::cout << "BC set to 0x1200\n";
    }
    break;
case 0x02:
    // LD (BC), A
    // Stores the contents of register A into the address in BC
    m_memory.writeMemory(m_BC.W, m_AF.B.B1);
    break;
case 0x03:
    // INC BC
    // Increments the B/C register pair
    m_BC.W++;
    break;
case 0x04:
    // INC B
    m_BC.B.B1++;
    m_AF.B.B0 = ((m_BC.B.B1 ? 0 : FLAG_Z) |
                 (m_BC.B.B1 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x05:
    // DEC B
    m_BC.B.B1--;
    m_AF.B.B0 = ((m_BC.B.B1 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_BC.B.B1 & 0x0F) == 0x0F ? FLAG_H : 0) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x06:
    // LD B,<byte>
    // Loads accumulator B with the next byte
    m_BC.B.B1 = m_memory.readMemory(m_PC.W++);
    break;
case 0x07:
    // RLCA
    // Rotate bits in accumulator A left 1 bit
    m_AF.B.B0 = m_AF.B.B1 & 0x80 ? FLAG_C : 0; // Check 8th bit to determine carry
    m_AF.B.B1 = (m_AF.B.B1 << 1) | (m_AF.B.B1 >> 7);
    break;
case 0x08:
    // LD (NNNN), SP
    // Loads the stack pointer into the given address
    tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
    tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);

    m_memory.writeMemory(tempRegister.W++, m_SP.B.B0);
    m_memory.writeMemory(tempRegister.W, m_SP.B.B1);
    break;
case 0x09:
    // ADD HL, BC
    tempRegister.W = (m_HL.W + m_BC.W) & 0xFFFF;
    m_AF.B.B0 = ((m_AF.B.B0 & FLAG_Z) |
                 ((m_HL.W^m_BC.W^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                 (((long)m_HL.W + (long)m_BC.W) & 0x10000 ? FLAG_C:0));
    m_HL.W = tempRegister.W;
    break;
case 0x0A:
    // LD A, (BC)
    // Loads the contents of the address in BC into accumulator A
    m_AF.B.B1 = m_memory.readMemory(m_BC.W);
    break;
case 0x0B:
    // DEC BC
    // Decrements the B/C register pair
    m_BC.W--;
    break;
case 0x0C:
    // INC C
    m_BC.B.B0++;
    m_AF.B.B0 = ((m_BC.B.B0 ? 0 : FLAG_Z) |
                 (m_BC.B.B0 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x0D:
    // DEC C
    m_BC.B.B0--;
    m_AF.B.B0 = ((m_BC.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_BC.B.B0 & 0x0F) == 0x0F ? FLAG_H : 0) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x0E:
    // LD C,<byte>
    // Loads accumulator C with the next byte
    m_BC.B.B0 = m_memory.readMemory(m_PC.W++);
    break;
case 0x0F:
    // RRCA
    // Rotate bits in accumulator A right by 1 bit
    tempValue = m_AF.B.B1 & 0x01;
    m_AF.B.B1 = (m_AF.B.B1 >> 1) | (tempValue ? 0x80 : 0);
    m_AF.B.B0 = (tempValue << 4);
    break;
case 0x10:
    // STOP
    // FIXME: I know we need to increment the PC, but is that all?
    m_PC.W++;
    break;
case 0x11:
    // LD DE, <word>
    // Loads the next word into the D/E register pair
    m_DE.B.B0 = m_memory.readMemory(m_PC.W++);
    m_DE.B.B1 = m_memory.readMemory(m_PC.W++);

    if (m_DE.W == 0x0) {
        std::cout << "LD DE, $0\n";
    }
    break;
case 0x12:
    // LD (DE), A
    // Stores the contents of register A into the address in DE
    m_memory.writeMemory(m_DE.W, m_AF.B.B1);
    break;
case 0x13:
    // INC DE
    // Increments the D/E register pair
    m_DE.W++;
    break;
case 0x14:
    // INC D
    m_DE.B.B1++;
    m_AF.B.B0 = ((m_DE.B.B1 ? 0 : FLAG_Z) |
                 (m_DE.B.B1 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x15:
    // DEC D
    m_DE.B.B1--;
    m_AF.B.B0 = ((m_DE.B.B1 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_DE.B.B1 & 0x0F) == 0x0F ? FLAG_H : 0) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x16:
    // LD D,<byte>
    // Loads accumulator D with the next byte
    m_DE.B.B1 = m_memory.readMemory(m_PC.W++);
    break;
case 0x17:
    // RLA
    tempValue = m_AF.B.B1 & 0x80 ? FLAG_C : 0; // Check 8th bit for FLAG_C
    m_AF.B.B1 = (m_AF.B.B1 << 1) | ((m_AF.B.B0 & FLAG_C) >> 4);
    m_AF.B.B0 = tempValue;
    break;
case 0x18:
    // JR <byte>
    // Jump by the displacement in the next byte
    m_PC.W += (s8)m_memory.readMemory(m_PC.W) + 1;
    break;
case 0x19:
    // ADD HL, DE
    // Add the contents of DE to HL
    tempRegister.W = (m_HL.W + m_DE.W) & 0xFFFF;
    m_AF.B.B0 = ((m_AF.B.B0 & FLAG_Z) |
                ((m_HL.W^m_DE.W^tempRegister.W) & 0x1000 ? FLAG_H:0)|
                (((long)m_HL.W + (long)m_DE.W) & 0x10000 ? FLAG_C:0));
    m_HL.W=tempRegister.W;
    break;
case 0x1A:
    // LD A, (DE)
    // Loads the contents of the address in DE into accumulator A
    m_AF.B.B1 = m_memory.readMemory(m_DE.W);
    break;
case 0x1B:
    // DEC DE
    // Decrements the D/E register pair
    m_DE.W--;
    break;
case 0x1C:
    // INC E
    m_DE.B.B0++;
    m_AF.B.B0 = ((m_DE.B.B0 ? 0 : FLAG_Z) |
                 (m_DE.B.B0 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x1D:
    // DEC E
    m_DE.B.B0--;
    m_AF.B.B0 = ((m_DE.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 (m_DE.B.B0 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x1E:
    // LD E,<byte>
    // Loads the next byte into E
    m_DE.B.B0 = m_memory.readMemory(m_PC.W++);
    break;
case 0x1F:
    // RRA
    // Rotate contents of accumulator A 1 bit right
    tempValue = m_AF.B.B1 & 0x01;
    m_AF.B.B1 = (m_AF.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
    m_AF.B.B0 = (tempValue << 4);
    break;
case 0x20:
    //JR NZ, <byte>
    // Relative jump if Z bit is not set (0)
    if (m_AF.B.B0 & FLAG_Z) {
        m_PC.W++;
    } else {
        m_PC.W += (s8)m_memory.readMemory(m_PC.W) + 1;
        cyclesSpent++;
    }
    break;
case 0x21:
    // LD HL, <word>
    // Loads register pair H/L with the next word
    m_HL.B.B0 = m_memory.readMemory(m_PC.W++);
    m_HL.B.B1 = m_memory.readMemory(m_PC.W++);
    break;
case 0x22:
    // LDI (HL),A
    // Load the value of accumulator A into the address in HL and increment HL
    m_memory.writeMemory(m_HL.W++, m_AF.B.B1);
    break;
case 0x23:
    // INC HL
    // Increments the H/L register pair
    m_HL.W++;
    break;
case 0x24:
    // INC H
    m_HL.B.B1++;
    m_AF.B.B0 = ((m_HL.B.B1 ? 0 : FLAG_Z) |
                 (m_HL.B.B1 & 0x0F ? 0: FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x25:
    // DEC H
    m_HL.B.B1--;
    m_AF.B.B0 = ((m_HL.B.B1 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_HL.B.B1 & 0x0F) == 0x0F ? FLAG_H : 0) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x26:
    // LD H,<byte>
    // Loads the next byte in to accumulator H
    m_HL.B.B1 = m_memory.readMemory(m_PC.W++);
    break;
case 0x27:
    // DAA
    tempRegister.W = m_AF.B.B1;
    if (m_AF.B.B0 & FLAG_C) tempRegister.W |= 256;
    if (m_AF.B.B0 & FLAG_H) tempRegister.W |= 512;
    if (m_AF.B.B0 & FLAG_N) tempRegister.W |= 1024;
    m_AF.W = DAATable[tempRegister.W];
    break;
case 0x28:
    // JR Z, <byte>
    // Relative jump by displacement if Z is set (1)
    if (m_AF.B.B0 & FLAG_Z) {
        m_PC.W += (s8)m_memory.readMemory(m_PC.W) + 1;
        cyclesSpent++;
    } else {
        m_PC.W++;
    }
    break;
case 0x29:
    // ADD HL,HL
    tempRegister.W = (m_HL.W+m_HL.W) & 0xFFFF;
    m_AF.B.B0= ((m_AF.B.B0 & FLAG_Z) |
                ((m_HL.W^m_HL.W^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                (((long)m_HL.W + (long)m_HL.W) & 0x10000 ? FLAG_C:0));
    m_HL.W = tempRegister.W;
    break;
case 0x2A:
    // LD A,(HL+)
    // Load A with the value from HL and then increment HL
    m_AF.B.B1 = m_memory.readMemory(m_HL.W++);
    break;
case 0x2B:
    // DEC HL
    // Decrements the H/L register pair
    m_HL.W--;
    break;
case 0x2C:
    // INC L
    m_HL.B.B0++;
    m_AF.B.B0 = ((m_HL.B.B0 ? 0 : FLAG_Z) |
                 (m_HL.B.B0 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x2D:
    // DEC L
    m_HL.B.B0++;
    m_AF.B.B0 = ((m_HL.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 (m_HL.B.B0 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x2E:
    // LD L, <byte>
    // Load accumulator L with the next byte
    m_HL.B.B0 = m_memory.readMemory(m_PC.W++);
    break;
case 0x2F:
    // CPL
    // One's complement on accumulator A
    m_AF.B.B1 ^= 255;
    m_AF.B.B0 |= FLAG_N | FLAG_H;
    break;
case 0x30:
    // JR NC, <byte>
    // Relative jump by the next byte when the C flag is not set (0)
    if (m_AF.B.B0 & FLAG_C) {
        m_PC.W++;
    } else {
        m_PC.W += (s8)m_memory.readMemory(m_PC.W) + 1;
        cyclesSpent++;
    }
    break;
case 0x31:
    // LD SP, <word>
    m_SP.B.B0 = m_memory.readMemory(m_PC.W++);
    m_SP.B.B1 = m_memory.readMemory(m_PC.W++);
    break;
case 0x32:
    // LDD (HL-),A
    // Load the value of accumulator A into the address in HL and decrement HL
    m_memory.writeMemory(m_HL.W--, m_AF.B.B1);
    break;
case 0x33:
    // INC SP
    // Increments the stack pointer
    m_SP.W++;
    break;
case 0x34:
    // INC (HL)
    // Increments the value in the H/L register
    tempValue = m_memory.readMemory(m_HL.W) + 1;
    m_AF.B.B0 = ((tempValue ? 0 : FLAG_Z) |
                 (tempValue & 0x0F ? 0: FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x35:
    // DEC (HL)
    // Decrements the value in the H/L register
    tempValue = m_memory.readMemory(m_HL.W) - 1;
    m_AF.B.B0 = ((tempValue ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((tempValue & 0x0F) == 0x0F ? FLAG_H : 0) |
                 (m_AF.B.B0 & FLAG_C));
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x36:
    // LD (HL), <byte>
    // Load the address pointed to by HL with the next byte
    m_memory.writeMemory(m_HL.W, m_memory.readMemory(m_PC.W++));
    break;
case 0x37:
    // SCF
    // Set Carry Flag
    m_AF.B.B0 = ((m_AF.B.B0 & FLAG_Z) | FLAG_C);
    break;
case 0x38:
    // JR C, <byte>
    // Relative jump by the next byte when the C flag is set (1)
    if (m_AF.B.B0 & FLAG_C) {
        m_PC.W += (s8)m_memory.readMemory(m_PC.W) + 1;
        cyclesSpent++;
    } else {
        m_PC.W++;
    }
    break;
case 0x39:
    // ADD HL,SP
    // Add the stack pointer to the H/L register pair
    tempRegister.W = (m_HL.W + m_SP.W) & 0xFFFF;
    m_AF.B.B0= ((m_AF.B.B0 & FLAG_Z) |
                ((m_HL.W^m_HL.W^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                (((long)m_HL.W + (long)m_HL.W) & 0x10000 ? FLAG_C:0));
    m_HL.W = tempRegister.W;
    break;
case 0x3A:
    // LD A, (HL-)
    // Load A with the value from the address in HL and increment HL
    m_AF.B.B1 = m_memory.readMemory(m_HL.W--);
    break;
case 0x3B:
    // DEC SP
    // Decrements the stack pointer
    m_SP.W--;
    break;
case 0x3C:
    // INC A
    m_AF.B.B1++;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 (m_AF.B.B1 & 0x0F ? 0:FLAG_H) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x3D:
    // DEC A
    m_AF.B.B1--;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1 & 0x0F) == 0x0F ? FLAG_H : 0) |
                 (m_AF.B.B0 & FLAG_C));
    break;
case 0x3E:
    // LD A, <byte>
    m_AF.B.B1 = m_memory.readMemory(m_PC.W++);
    break;
case 0x3F:
    // CCF
    // Complement Carry Flag
    m_AF.B.B0 ^= FLAG_C;             // Invert C flag
    m_AF.B.B0 &= ~(FLAG_N | FLAG_H); // Unset N and H flags
    break;
case 0x40:
    // LD B,B
    // Already equal, so we don't have to do anything
    break;
case 0x41:
    // LD B,C
    // Load B with C
    m_BC.B.B1 = m_BC.B.B0;
    break;
case 0x42:
    // LD B,D
    m_BC.B.B1 = m_DE.B.B1;
    break;
case 0x43:
    // LD B,E
    m_BC.B.B1 = m_DE.B.B0;
    break;
case 0x44:
    // LD B,H
    m_BC.B.B1 = m_HL.B.B1;
    break;
case 0x45:
    // LD B,L
    m_BC.B.B1 = m_HL.B.B0;
    break;
case 0x46:
    // LD B, (HL)
    // Load B with the value pointed to by HL
    m_BC.B.B1 = m_memory.readMemory(m_HL.W);
    break;
case 0x47:
    // LD B, A
    m_BC.B.B1 = m_AF.B.B1;
    break;
case 0x48:
    // LD C,B
    m_BC.B.B0 = m_BC.B.B1;
    break;
case 0x49:
    // LD C,C
    // Already equal, we don't have to do anything to emulate this
    break;
case 0x4A:
    // LD C, D
    m_BC.B.B0 = m_DE.B.B1;
    break;
case 0x4B:
    // LD C, E
    m_BC.B.B0 = m_DE.B.B0;
    break;
case 0x4C:
    // LD C, H
    m_BC.B.B0 = m_HL.B.B1;
    break;
case 0x4D:
    // LD C, L
    m_BC.B.B0 = m_HL.B.B0;
    break;
case 0x4E:
    // LD C, (HL)
    // Load C with the value pointed to by HL
    m_BC.B.B0 = m_memory.readMemory(m_HL.W);
    break;
case 0x4F:
    // LD C, A
    m_BC.B.B0 = m_AF.B.B1;
    break;
case 0x50:
    // LD D, B
    m_DE.B.B1 = m_BC.B.B1;
    break;
case 0x51:
    // LD D, C
    m_DE.B.B1 = m_BC.B.B0;
    break;
case 0x52:
    // LD D, D
    // Don't have to do anything to emulate this
    break;
case 0x53:
    // LD D, E
    m_DE.B.B1 = m_DE.B.B0;
    break;
case 0x54:
    // LD D, H
    m_DE.B.B1 = m_HL.B.B1;
    break;
case 0x55:
    // LD D, L
    m_DE.B.B1 = m_HL.B.B0;
    break;
case 0x56:
    // LD D, (HL)
    m_DE.B.B1 = m_memory.readMemory(m_HL.W);
    break;
case 0x57:
    // LD D, A
    m_DE.B.B1 = m_AF.B.B1;
    break;
case 0x58:
    // LD E, B
    m_DE.B.B0 = m_BC.B.B1;
    break;
case 0x59:
    // LD E, C
    m_DE.B.B0 = m_BC.B.B0;
    break;
case 0x5A:
    // LD E, D
    m_DE.B.B0 = m_DE.B.B1;
    break;
case 0x5B:
    // LD E, E
    break;
case 0x5C:
    // LD E, H
    m_DE.B.B0 = m_HL.B.B1;
    break;
case 0x5D:
    // LD E, L
    m_DE.B.B0 = m_HL.B.B0;
    break;
case 0x5E:
    // LD E, (HL)
    m_DE.B.B0 = m_memory.readMemory(m_HL.W);
    break;
case 0x5F:
    // LD E, A
    m_DE.B.B0 = m_AF.B.B1;
    break;
case 0x60:
    // LD H, B
    m_HL.B.B1 = m_BC.B.B1;
    break;
case 0x61:
    // LD H, C
    m_HL.B.B1 = m_BC.B.B0;
    break;
case 0x62:
    // LD H, D
    m_HL.B.B1 = m_DE.B.B1;
    break;
case 0x63:
    // LD H, E
    m_HL.B.B1 = m_DE.B.B0;
    break;
case 0x64:
    // LD H, H
    break;
case 0x65:
    // LD H, L
    m_HL.B.B1 = m_HL.B.B0;
    break;
case 0x66:
    // LD H, (HL)
    m_HL.B.B1 = m_memory.readMemory(m_HL.W);
    break;
case 0x67:
    // LD H, A
    m_HL.B.B1 = m_AF.B.B1;
    break;
case 0x68:
    // LD L, B
    m_HL.B.B0 = m_BC.B.B1;
    break;
case 0x69:
    // LD L, C
    m_HL.B.B0 = m_BC.B.B0;
    break;
case 0x6A:
    // LD L, D
    m_HL.B.B0 = m_DE.B.B1;
    break;
case 0x6B:
    // LD L, E
    m_HL.B.B0 = m_DE.B.B0;
    break;
case 0x6C:
    // LD L, H
    m_HL.B.B0 = m_HL.B.B1;
    break;
case 0x6D:
    // LD L, L
    break;
case 0x6E:
    // LD L, (HL)
    m_HL.B.B0 = m_memory.readMemory(m_HL.W);
    break;
case 0x6F:
    // LD L, A
    m_HL.B.B0 = m_AF.B.B1;
    break;
case 0x70:
    // LD (HL), B
    // Load the value pointed to by HL with the contents of register B
    m_memory.writeMemory(m_HL.W, m_BC.B.B1);
    break;
case 0x71:
    // LD (HL), C
    m_memory.writeMemory(m_HL.W, m_BC.B.B0);
    break;
case 0x72:
    // LD (HL), D
    m_memory.writeMemory(m_HL.W, m_DE.B.B1);
    break;
case 0x73:
    // LD (HL), E
    m_memory.writeMemory(m_HL.W, m_DE.B.B0);
    break;
case 0x74:
    // LD (HL), H
    m_memory.writeMemory(m_HL.W, m_HL.B.B1);
    break;
case 0x75:
    // LD (HL), L
    m_memory.writeMemory(m_HL.W, m_HL.B.B0);
    break;
case 0x76:
    // HALT
    // FIXME: If an EI is pending, the interrupts are triggered before Halt state
    m_halt = true;
    break;
case 0x77:
    // LD (HL), A
    m_memory.writeMemory(m_HL.W, m_AF.B.B1);
    break;
case 0x78:
    // LD A, B
    m_AF.B.B1 = m_BC.B.B1;
    break;
case 0x79:
    // LD A, C
    m_AF.B.B1 = m_BC.B.B0;
    break;
case 0x7A:
    // LD A, D
    m_AF.B.B1 = m_DE.B.B1;
    break;
case 0x7B:
    // LD A, E
    m_AF.B.B1 = m_DE.B.B0;
    break;
case 0x7C:
    // LD A, H
    m_AF.B.B1 = m_HL.B.B1;
    break;
case 0x7D:
    // LD A, L
    m_AF.B.B1 = m_HL.B.B0;
    break;
case 0x7E:
    // LD A, (HL)
    m_AF.B.B1 = m_memory.readMemory(m_HL.W);
    break;
case 0x7F:
    // LD A, A
    break;
case 0x80:
    // ADD A, B
    tempRegister.W = m_AF.B.B1 + m_BC.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_BC.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x81:
    // ADD A, C
    tempRegister.W = m_AF.B.B1 + m_BC.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_BC.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x82:
    // ADD A, D
    tempRegister.W = m_AF.B.B1 + m_DE.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_DE.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x83:
    // ADD A, E
    tempRegister.W = m_AF.B.B1 + m_DE.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_DE.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x84:
    // ADD A, H
    tempRegister.W = m_AF.B.B1 + m_HL.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_HL.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x85:
    // ADD A, L
    tempRegister.W = m_AF.B.B1 + m_HL.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_HL.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x86:
    // ADD A, (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempRegister.W = m_AF.B.B1 + tempValue;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x87:
    // ADD A, A
    tempRegister.W = m_AF.B.B1 + m_AF.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_AF.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x88:
    // ADC A, B
    tempRegister.W = m_AF.B.B1 + m_BC.B.B1 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_BC.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x89:
    // ADC A, C
    tempRegister.W = m_AF.B.B1 + m_BC.B.B0 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_BC.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x8A:
    // ADC A, D
    tempRegister.W = m_AF.B.B1 + m_DE.B.B1 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_DE.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x8B:
    // ADC A, E
    tempRegister.W = m_AF.B.B1 + m_DE.B.B0 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_DE.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x8C:
    // ADC A, H
    tempRegister.W = m_AF.B.B1 + m_HL.B.B1 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_HL.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x8D:
    // ADC A, L
    tempRegister.W = m_AF.B.B1 + m_HL.B.B0 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_HL.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x8E:
    // ADC A, (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempRegister.W = m_AF.B.B1 + tempValue + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x8F:
    // ADC A, A
    tempRegister.W = m_AF.B.B1 + m_AF.B.B1 + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^m_AF.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x90:
    // SUB B
    tempRegister.W = m_AF.B.B1 - m_BC.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_BC.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x91:
    // SUB C
    tempRegister.W = m_AF.B.B1 - m_BC.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_BC.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x92:
    // SUB D
    tempRegister.W = m_AF.B.B1 - m_DE.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_DE.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x93:
    // SUB E
    tempRegister.W = m_AF.B.B1 - m_DE.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_DE.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B0 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x94:
    // SUB H
    tempRegister.W = m_AF.B.B1 - m_HL.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_HL.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x95:
    // SUB L
    tempRegister.W = m_AF.B.B1 - m_HL.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_HL.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x96:
    // SUB (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempRegister.W = m_AF.B.B1 - tempValue;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x97:
    // SUB A
    m_AF.B.B1 = 0;
    m_AF.B.B0 = FLAG_N | FLAG_Z;
    break;
case 0x98:
    // SBC A, B
    tempRegister.W = m_AF.B.B1 - m_BC.B.B1 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_BC.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x99:
    // SBC A, C
    tempRegister.W = m_AF.B.B1 - m_BC.B.B0 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_BC.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x9A:
    // SBC A, D
    tempRegister.W = m_AF.B.B1 - m_DE.B.B1 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_DE.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x9B:
    // SBC A, E
    tempRegister.W = m_AF.B.B1 - m_DE.B.B0 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_DE.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x9C:
    // SBC A, H
    tempRegister.W = m_AF.B.B1 - m_HL.B.B1 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_HL.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x9D:
    // SBC A, L
    tempRegister.W = m_AF.B.B1 - m_HL.B.B0 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_HL.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x9E:
    // SBC A, (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempRegister.W = m_AF.B.B1 - tempValue - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0x9F:
    // SBC A, A
    tempRegister.W = m_AF.B.B1 - m_AF.B.B1 - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_AF.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0xA0:
    // AND B
    m_AF.B.B1 &= m_BC.B.B1;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA1:
    // AND C
    m_AF.B.B1 &= m_BC.B.B0;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA2:
    // AND D
    m_AF.B.B1 &= m_DE.B.B1;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA3:
    // AND E
    m_AF.B.B1 &= m_DE.B.B0;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA4:
    // AND H
    m_AF.B.B1 &= m_HL.B.B1;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA5:
    // AND L
    m_AF.B.B1 &= m_HL.B.B0;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA6:
    // AND (HL)
    m_AF.B.B1 &= m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA7:
    // AND A
    m_AF.B.B1 &= m_AF.B.B1;
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xA8:
    // XOR B
    m_AF.B.B1 ^= m_BC.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xA9:
    // XOR C
    m_AF.B.B1 ^= m_BC.B.B0;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xAA:
    // XOR D
    m_AF.B.B1 ^= m_DE.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xAB:
    // XOR E
    m_AF.B.B1 ^= m_DE.B.B0;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xAC:
    // XOR H
    m_AF.B.B1 ^= m_HL.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xAD:
    // XOR L
    m_AF.B.B1 ^= m_HL.B.B0;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xAE:
    // XOR (HL)
    m_AF.B.B1 ^= m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xAF:
    // XOR A
    m_AF.B.B1 = 0;
    m_AF.B.B0 = FLAG_Z;
    break;
case 0xB0:
    // OR B
    m_AF.B.B1 |= m_BC.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB1:
    // OR C
    m_AF.B.B1 |= m_BC.B.B0;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB2:
    // OR D
    m_AF.B.B1 |= m_DE.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB3:
    // OR E
    m_AF.B.B1 |= m_DE.B.B0;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB4:
    // OR H
    m_AF.B.B1 |= m_HL.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB5:
    // OR L
    m_AF.B.B1 |= m_HL.B.B0;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB6:
    // OR (HL)
    m_AF.B.B1 |= m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB7:
    // OR A
    m_AF.B.B1 |= m_AF.B.B1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xB8:
    // CP B
    tempRegister.W = m_AF.B.B1 - m_BC.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_BC.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xB9:
    // CP C
    tempRegister.W = m_AF.B.B1 - m_BC.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_BC.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xBA:
    // CP D
    tempRegister.W = m_AF.B.B1 - m_DE.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_DE.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xBB:
    // CP E
    tempRegister.W = m_AF.B.B1 - m_DE.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_DE.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    //fprintf(stderr, "CP E, is zero?: %d\n", tempRegister.B.B0);
    break;
case 0xBC:
    // CP H
    tempRegister.W = m_AF.B.B1 - m_HL.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_HL.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xBD:
    // CP L
    tempRegister.W = m_AF.B.B1 - m_HL.B.B0;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_HL.B.B0^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xBE:
    // CP (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempRegister.W = m_AF.B.B1 - tempValue;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xBF:
    // CP A
    tempRegister.W = m_AF.B.B1 - m_AF.B.B1;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^m_AF.B.B1^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xC0:
    // RET NZ
    // Return from CALL if the Z bit is not set (0)
    if(!(m_AF.B.B0 & FLAG_Z)) {
        m_PC.B.B0 = m_memory.readMemory(m_SP.W++);
        m_PC.B.B1 = m_memory.readMemory(m_SP.W++);
        cyclesSpent += 3;
    }
    break;
case 0xC1:
    // POP BC
    m_BC.B.B0 = m_memory.readMemory(m_SP.W++);
    m_BC.B.B1 = m_memory.readMemory(m_SP.W++);
    break;
case 0xC2:
    // JP NZ, <word>
    // Jump to the address in the next word if the Z bit is not set (0)
    if(m_AF.B.B0 & FLAG_Z) {
        m_PC.W += 2;
    } else {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W);
        m_PC.W = tempRegister.W;
        cyclesSpent++;
    }
    break;
case 0xC3:
    // JP <word>
    tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
    tempRegister.B.B1 = m_memory.readMemory(m_PC.W);
    m_PC.W = tempRegister.W;
    break;
case 0xC4:
    // CALL NZ, <word>
    if(m_AF.B.B0 & FLAG_Z) {
        m_PC.W += 2;
    } else {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);

        m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
        m_memory.writeMemory(--m_SP.W, m_PC.B.B0);

        m_PC.W = tempRegister.W;
        cyclesSpent += 3;
    }
    break;
case 0xC5:
    // PUSH BC
    m_memory.writeMemory(--m_SP.W, m_BC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_BC.B.B0);
    break;
case 0xC6:
    // ADD A, <byte>
    tempValue = m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_AF.B.B1 + tempValue;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0xC7:
    // RST 00
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0000;
    break;
case 0xC8:
    // RET Z
    // Return from CALL if the Z bit is set (1)
    if(m_AF.B.B0 & FLAG_Z) {
        m_PC.B.B0 = m_memory.readMemory(m_SP.W++);
        m_PC.B.B1 = m_memory.readMemory(m_SP.W++);
        cyclesSpent += 3;
    }
    break;
case 0xC9:
    // RET
    m_PC.B.B0 = m_memory.readMemory(m_SP.W++);
    m_PC.B.B1 = m_memory.readMemory(m_SP.W++);
    break;
case 0xCA:
    // JP Z, <word>
    // Jump by the next word if the Z bit is set (1)
    if(m_AF.B.B0 & FLAG_Z) {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W);
        m_PC.W = tempRegister.W;
        cyclesSpent++;
    } else {
        m_PC.W += 2;
    }
    break;
    // 0xCB handled in OpcodesCB.h
case 0xCC:
    // CALL Z, <word>
    if(m_AF.B.B0 & FLAG_Z) {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);

        m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
        m_memory.writeMemory(--m_SP.W, m_PC.B.B0);

        m_PC.W = tempRegister.W;
        cyclesSpent += 3;
    } else {
        m_PC.W += 2;
    }
    break;
case 0xCD:
    // CALL <word>
    tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
    tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);

    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);

    m_PC.W = tempRegister.W;
    break;
case 0xCE:
    // ADC A, <byte>
    tempValue = m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_AF.B.B1 + tempValue + (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0xCF:
    // RST 08
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0008;
    break;
case 0xD0:
    // RET NC
    // Return from CALL if the C bit is not set (0)
    if(!(m_AF.B.B0 & FLAG_C)) {
        m_PC.B.B0 = m_memory.readMemory(m_SP.W++);
        m_PC.B.B1 = m_memory.readMemory(m_SP.W++);
        cyclesSpent += 3;
    }
    break;
case 0xD1:
    // POP DE
    m_DE.B.B0 = m_memory.readMemory(m_SP.W++);
    m_DE.B.B1 = m_memory.readMemory(m_SP.W++);
    break;
case 0xD2:
    // JP NC, <word>
    // Jump to the address in the next word if the C bit is not set (0)
    if(m_AF.B.B0 & FLAG_C) {
        m_PC.W += 2;
    } else {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W);
        m_PC.W = tempRegister.W;
        cyclesSpent++;
    }
    break;
case 0xD3: // Illegal
    assert(false);
    break;
case 0xD4:
    // CALL NC, <word>
    if(m_AF.B.B0 & FLAG_C) {
        m_PC.W += 2;
    } else {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);

        m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
        m_memory.writeMemory(--m_SP.W, m_PC.B.B0);

        m_PC.W = tempRegister.W;
        cyclesSpent += 3;
    }
    break;
case 0xD5:
    // PUSH DE
    m_memory.writeMemory(--m_SP.W, m_DE.B.B1);
    m_memory.writeMemory(--m_SP.W, m_DE.B.B0);
    break;
case 0xD6:
    // SUB <byte>
    tempValue = m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_AF.B.B1 - tempValue;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0xD7:
    // RST 10
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0010;
    break;
case 0xD8:
    // RET C
    // Return from CALL if the C bit is set (1)
    if(m_AF.B.B0 & FLAG_C) {
        m_PC.B.B0 = m_memory.readMemory(m_SP.W++);
        m_PC.B.B1 = m_memory.readMemory(m_SP.W++);
        cyclesSpent += 3;
    }
    break;
case 0xD9:
    // RETI
    // Return from Interrupt
    m_PC.B.B0 = m_memory.readMemory(m_SP.W++);
    m_PC.B.B1 = m_memory.readMemory(m_SP.W++);
    m_IFF = 1;
    break;
case 0xDA:
    // JP C, <word>
    // Jump by the next word if the C bit is set (1)
    if(m_AF.B.B0 & FLAG_C) {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W);
        m_PC.W = tempRegister.W;
        cyclesSpent++;
    } else {
        m_PC.W += 2;
    }
    break;
case 0xDB: // Illegal
    assert(false);
    break;
case 0xDC:
    // CALL C, <word>
    if(m_AF.B.B0 & FLAG_C) {
        tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
        tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);

        m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
        m_memory.writeMemory(--m_SP.W, m_PC.B.B0);

        m_PC.W = tempRegister.W;
        cyclesSpent += 3;
    } else {
        m_PC.W += 2;
    }
    break;
case 0xDD: // Illegal
    break;
case 0xDE:
    // SBC A, <byte>
    tempValue = m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_AF.B.B1 - tempValue - (m_AF.B.B0 & FLAG_C ? 1 : 0);
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    m_AF.B.B1 = tempRegister.B.B0;
    break;
case 0xDF:
    // RST 18
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0018;
    break;
case 0xE0:
    // LDH (FF00+<byte>), A
    // Load from high memory into A
    m_memory.writeMemory(0xFF00 | m_memory.readMemory(m_PC.W++), m_AF.B.B1);
    break;
case 0xE1:
    // POP HL
    m_HL.B.B0 = m_memory.readMemory(m_SP.W++);
    m_HL.B.B1 = m_memory.readMemory(m_SP.W++);
    break;
case 0xE2:
    // LD (C), A
    // Load the high memory address in C with the contents of A
    m_memory.writeMemory(0xFF00 | m_BC.B.B0, m_AF.B.B1);
    break;
case 0xE3: // Illegal
case 0xE4: // Illegal
    assert(false);
    break;
case 0xE5:
    // PUSH HL
    m_memory.writeMemory(--m_SP.W, m_HL.B.B1);
    m_memory.writeMemory(--m_SP.W, m_HL.B.B0);
    break;
case 0xE6:
    // AND <byte> (implicit compare to A register)
    m_AF.B.B1 &= m_memory.readMemory(m_PC.W++);
    m_AF.B.B0 = ((m_AF.B.B1 ? 0 : FLAG_Z) |
                 FLAG_H);
    break;
case 0xE7:
    // RST 20
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0020;
    break;
case 0xE8:
    // ADD SP, <byte>
    offset = (s8)m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_SP.W + offset;

    if(offset >= 0) {
        m_AF.B.B0 = (((m_SP.W^offset^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                     (m_SP.W > tempRegister.W ? FLAG_C : 0));
    } else {
        m_AF.B.B0 = (((m_SP.W^offset^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                     (m_SP.W < tempRegister.W ? FLAG_C : 0));
    }

    m_SP.W = tempRegister.W;
    break;
case 0xE9:
    // JP (HL)
    m_PC.W = m_HL.W;
    break;
case 0xEA:
    // LD <word>, A
    tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
    tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);
    m_memory.writeMemory(tempRegister.W, m_AF.B.B1);
    break;
case 0xEB: // Illegal
case 0xEC: // Illegal
case 0xED: // Illegal
    assert(false);
    break;
case 0xEE:
    // XOR <byte>
    m_AF.B.B1 ^= m_memory.readMemory(m_PC.W++);
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xEF:
    // RST 28
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0028;
    break;
case 0xF0:
    // LDH A, <byte>
    // Load A with the hi-mem address in the next byte
    m_AF.B.B1 = m_memory.readMemory(0xFF00 | m_memory.readMemory(m_PC.W++));
    break;
case 0xF1:
    // POP AF
    // Can't assign anything to the lower nibble of the flags register
    m_AF.B.B0 = 0xF0 & m_memory.readMemory(m_SP.W++);
    m_AF.B.B1 = m_memory.readMemory(m_SP.W++);
    break;
case 0xF2:
    // LD A, (C)
    // Load A with the hi-mem address in C
    m_AF.B.B1 = m_memory.readMemory(0xFF00 + m_BC.B.B0);
    break;
case 0xF3:
    // DI
    m_IFF = 0;
    break;
case 0xF4: // Illegal
    break;
case 0xF5:
    // PUSH AF
    m_memory.writeMemory(--m_SP.W, m_AF.B.B1);
    m_memory.writeMemory(--m_SP.W, m_AF.B.B0);
    break;
case 0xF6:
    // OR <byte>
    m_AF.B.B1 |= m_memory.readMemory(m_PC.W++);
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0xF7:
    // RST 30
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0030;
    break;
case 0xF8:
    // LD HL, SP+<byte>
    offset = (s8)m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_SP.W + offset;

    if(offset >= 0) {
        m_AF.B.B0 = (((m_SP.W^offset^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                     (m_SP.W > tempRegister.W ? FLAG_C : 0));
    } else {
        m_AF.B.B0 = (((m_SP.W^offset^tempRegister.W) & 0x1000 ? FLAG_H : 0) |
                     (m_SP.W < tempRegister.W ? FLAG_C : 0));
    }

    m_HL.W = tempRegister.W;
    break;
case 0xF9:
    // LD SP,HL
    m_SP.W = m_HL.W;
    break;
case 0xFA:
    // LD A, (<word>)
    // Load A with the value of the address pointed to by the following word
    tempRegister.B.B0 = m_memory.readMemory(m_PC.W++);
    tempRegister.B.B1 = m_memory.readMemory(m_PC.W++);
    m_AF.B.B1 = m_memory.readMemory(tempRegister.W);
    break;
case 0xFB:
    // EI
    m_setIFF = true;
    break;
case 0xFC: // Illegal
case 0xFD: // Illegal
    break;
case 0xFE:
    // CP <byte>
    tempValue = m_memory.readMemory(m_PC.W++);
    tempRegister.W = m_AF.B.B1 - tempValue;
    m_AF.B.B0 = ((tempRegister.B.B0 ? 0 : FLAG_Z) |
                 FLAG_N |
                 ((m_AF.B.B1^tempValue^tempRegister.B.B0) & 0x10 ? FLAG_H:0) |
                 (tempRegister.B.B1 ? FLAG_C : 0));
    break;
case 0xFF:
    // RST 38
    std::cout << "Hitting RST 38\n";
    m_memory.writeMemory(--m_SP.W, m_PC.B.B1);
    m_memory.writeMemory(--m_SP.W, m_PC.B.B0);
    m_PC.W = 0x0038;
    break;
default:
    // Unknown opcode
    std::cout << "Hit unknown opcode: " << opcode1 << "\n";
    break;
}

#endif
