/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

// Only intended to be included from CPU::run(). This switch is in a separate
// file to help maintain readability in the CPU class

#ifndef OPCODESCB_H
#define OPCODESCB_H

// Condition flag #defines located in CPU.h

switch (opcode2) {
case 0x00:
    // RLC B
    m_AF.B.B0 = (m_BC.B.B1 & 0x80) ? FLAG_C : 0;
    m_BC.B.B1 = (m_BC.B.B1 << 1) | (m_BC.B.B1 >> 7);
    m_AF.B.B0 |= m_BC.B.B1 ? 0 : FLAG_Z;
    break;
case 0x01:
    // RLC C
    m_AF.B.B0 = (m_BC.B.B0 & 0x80) ? FLAG_C : 0;
    m_BC.B.B0 = (m_BC.B.B0 << 1) | (m_BC.B.B0 >> 7);
    m_AF.B.B0 |= m_BC.B.B0 ? 0 : FLAG_Z;
    break;
case 0x02:
    // RLC D
    m_AF.B.B0 = (m_DE.B.B1 & 0x80) ? FLAG_C : 0;
    m_DE.B.B1 = (m_DE.B.B1 << 1) | (m_DE.B.B1 >> 7);
    m_AF.B.B0 |= m_DE.B.B1 ? 0 : FLAG_Z;
    break;
case 0x03:
    // RLC E
    m_AF.B.B0 = (m_DE.B.B0 & 0x80) ? FLAG_C : 0;
    m_DE.B.B0 = (m_DE.B.B0 << 1) | (m_DE.B.B0 >> 7);
    m_AF.B.B0 |= m_DE.B.B0 ? 0 : FLAG_Z;
    break;
case 0x04:
    // RLC H
    m_AF.B.B0 = (m_HL.B.B1 & 0x80) ? FLAG_C : 0;
    m_HL.B.B1 = (m_HL.B.B1 << 1) | (m_HL.B.B1 >> 7);
    m_AF.B.B0 |= m_HL.B.B1 ? 0 : FLAG_Z;
    break;
case 0x05:
    // RLC L
    m_AF.B.B0 = (m_HL.B.B0 & 0x80) ? FLAG_C : 0;
    m_HL.B.B0 = (m_HL.B.B0 << 1) | (m_HL.B.B0 >> 7);
    m_AF.B.B0 |= m_HL.B.B0 ? 0 : FLAG_Z;
    break;
case 0x06:
    // RLC (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & 0x80) ? FLAG_C : 0;
    tempValue = (tempValue << 1) | (tempValue >> 7);
    m_AF.B.B0 |= tempValue ? 0 : FLAG_Z;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x07:
    // RLC A
    m_AF.B.B0 = (m_AF.B.B1 & 0x80) ? FLAG_C : 0;
    m_AF.B.B1 = (m_AF.B.B1 << 1) | (m_AF.B.B1 >> 7);
    m_AF.B.B0 |= m_AF.B.B1 ? 0 : FLAG_Z;
    break;
case 0x08:
    // RRC B
    m_AF.B.B0 = (m_BC.B.B1 & 0x01) ? FLAG_C : 0;
    m_BC.B.B1 = (m_BC.B.B1 >> 1) | (m_BC.B.B1 << 7);
    m_AF.B.B0 |= m_BC.B.B1 ? 0 : FLAG_Z;
    break;
case 0x09:
    // RRC C
    m_AF.B.B0 = (m_BC.B.B0 & 0x01) ? FLAG_C : 0;
    m_BC.B.B0 = (m_BC.B.B0 >> 1) | (m_BC.B.B0 << 7);
    m_AF.B.B0 |= m_BC.B.B0 ? 0 : FLAG_Z;
    break;
case 0x0A:
    // RRC D
    m_AF.B.B0 = (m_DE.B.B1 & 0x01) ? FLAG_C : 0;
    m_DE.B.B1 = (m_DE.B.B1 >> 1) | (m_DE.B.B1 << 7);
    m_AF.B.B0 |= m_DE.B.B1 ? 0 : FLAG_Z;
    break;
case 0x0B:
    // RRC E
    m_AF.B.B0 = (m_DE.B.B0 & 0x01) ? FLAG_C : 0;
    m_DE.B.B0 = (m_DE.B.B0 >> 1) | (m_DE.B.B0 << 7);
    m_AF.B.B0 |= m_DE.B.B0 ? 0 : FLAG_Z;
    break;
case 0x0C:
    // RRC H
    m_AF.B.B0 = (m_HL.B.B1 & 0x01) ? FLAG_C : 0;
    m_HL.B.B1 = (m_HL.B.B1 >> 1) | (m_HL.B.B1 << 7);
    m_AF.B.B0 |= m_HL.B.B1 ? 0 : FLAG_Z;
    break;
case 0x0D:
    // RRC L
    m_AF.B.B0 = (m_HL.B.B0 & 0x01) ? FLAG_C : 0;
    m_HL.B.B0 = (m_HL.B.B0 >> 1) | (m_HL.B.B0 << 7);
    m_AF.B.B0 |= m_HL.B.B0 ? 0 : FLAG_Z;
    break;
case 0x0E:
    // RRC (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & 0x01) ? FLAG_C : 0;
    tempValue = (tempValue >> 1) | (tempValue << 7);
    m_AF.B.B0 |= tempValue ? 0 : FLAG_Z;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x0F:
    // RRC A
    m_AF.B.B0 = (m_AF.B.B1 & 0x01) ? FLAG_C : 0;
    m_AF.B.B1 = (m_AF.B.B1 >> 1) | (m_AF.B.B1 << 7);
    m_AF.B.B0 |= m_AF.B.B1 ? 0 : FLAG_Z;
    break;
case 0x10:
    // RL B
    if (BIT7(m_BC.B.B1)) {
        m_BC.B.B1 = (m_BC.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_BC.B.B1 = (m_BC.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x11:
    // RC C
    if (BIT7(m_BC.B.B0)) {
        m_BC.B.B0 = (m_BC.B.B0 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_BC.B.B0 = (m_BC.B.B0 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z);
    }
    break;
case 0x12:
    // RC D
    if (BIT7(m_DE.B.B1)) {
        m_DE.B.B1 = (m_DE.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_DE.B.B1 = (m_DE.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x13:
    // RC E
    if (BIT7(m_DE.B.B0)) {
        m_DE.B.B0 = (m_DE.B.B0 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_DE.B.B0 = (m_DE.B.B0 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z);
    }
    break;
case 0x14:
    // RC H
    if (BIT7(m_HL.B.B1)) {
        m_HL.B.B1 = (m_HL.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_HL.B.B1 = (m_HL.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x15:
    // RC L
    if (BIT7(m_HL.B.B0)) {
        m_HL.B.B0 = (m_HL.B.B0 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_HL.B.B0 = (m_HL.B.B0 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z);
    }
    break;
case 0x16:
    // RC (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    if (BIT7(tempValue)) {
        tempValue = (tempValue << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (tempValue ? 0 : FLAG_Z) | FLAG_C;
    } else {
        tempValue = (tempValue << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (tempValue ? 0 : FLAG_Z);
    }
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x17:
    // RC A
    if (BIT7(m_AF.B.B1)) {
        m_AF.B.B1 = (m_AF.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_AF.B.B1 = (m_AF.B.B1 << 1) | (m_AF.B.B0 & FLAG_C ? 1 : 0);
        m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x18:
    // RR B
    if(BIT0(m_BC.B.B1)) {
        m_BC.B.B1 = (m_BC.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_BC.B.B1 = (m_BC.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x19:
    // RR C
    if(BIT0(m_BC.B.B0)) {
        m_BC.B.B0 = (m_BC.B.B0 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_BC.B.B0 = (m_BC.B.B0 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z);
    }
    break;
case 0x1A:
    // RR D
    if(BIT0(m_DE.B.B1)) {
        m_DE.B.B1 = (m_DE.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_DE.B.B1 = (m_DE.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x1B:
    // RR E
    if(BIT0(m_DE.B.B0)) {
        m_DE.B.B0 = (m_DE.B.B0 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_DE.B.B0 = (m_DE.B.B0 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z);
    }
    break;
case 0x1C:
    // RR H
    if(BIT0(m_HL.B.B1)) {
        m_HL.B.B1 = (m_HL.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_HL.B.B1 = (m_HL.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x1D:
    // RR L
    if(BIT0(m_HL.B.B0)) {
        m_HL.B.B0 = (m_HL.B.B0 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_HL.B.B0 = (m_HL.B.B0 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z);
    }
    break;
case 0x1E:
    // RL (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    if(BIT0(tempValue)) {
        tempValue = (tempValue >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (tempValue ? 0 : FLAG_Z) | FLAG_C;
    } else {
        tempValue = (tempValue >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (tempValue ? 0 : FLAG_Z);
    }
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x1F:
    // RR A
    if(BIT0(m_AF.B.B1)) {
        m_AF.B.B1 = (m_AF.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z) | FLAG_C;
    } else {
        m_AF.B.B1 = (m_AF.B.B1 >> 1) | (m_AF.B.B0 & FLAG_C ? 0x80 : 0);
        m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    }
    break;
case 0x20:
    // SLA B
    m_AF.B.B0 = (BIT7(m_BC.B.B1) ? FLAG_C : 0);
    m_BC.B.B1 <<= 1;
    m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z);
    break;
case 0x21:
    // SLA C
    m_AF.B.B0 = (BIT7(m_BC.B.B0) ? FLAG_C : 0);
    m_BC.B.B0 <<= 1;
    m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z);
    break;
case 0x22:
    // SLA D
    m_AF.B.B0 = (BIT7(m_DE.B.B1) ? FLAG_C : 0);
    m_DE.B.B1 <<= 1;
    m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z);
    break;
case 0x23:
    // SLA E
    m_AF.B.B0 = (BIT7(m_DE.B.B0) ? FLAG_C : 0);
    m_DE.B.B0 <<= 1;
    m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z);
    break;
case 0x24:
    // SLA H
    m_AF.B.B0 = (BIT7(m_HL.B.B1) ? FLAG_C : 0);
    m_HL.B.B1 <<= 1;
    m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z);
    break;
case 0x25:
    // SLA L
    m_AF.B.B0 = (BIT7(m_HL.B.B0) ? FLAG_C : 0);
    m_HL.B.B0 <<= 1;
    m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z);
    break;
case 0x26:
    // SLA (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (BIT7(tempValue) ? FLAG_C : 0);
    tempValue <<= 1;
    m_AF.B.B0 = (tempValue ? 0 : FLAG_Z);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x27:
    // SLA A
    m_AF.B.B0 = (BIT7(m_AF.B.B1) ? FLAG_C : 0);
    m_AF.B.B1 <<= 1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0x28:
    // SRA B
    m_AF.B.B0 = (BIT1(m_BC.B.B1) ? FLAG_C : 0);
    m_BC.B.B1 = (m_BC.B.B1 >> 1) | (BIT7(m_BC.B.B1));
    m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z);
    break;
case 0x29:
    // SRA C
    m_AF.B.B0 = (BIT1(m_BC.B.B0) ? FLAG_C : 0);
    m_BC.B.B0 = (m_BC.B.B0 >> 1) | (BIT7(m_BC.B.B0));
    m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z);
    break;
case 0x2A:
    // SRA D
    m_AF.B.B0 = (BIT1(m_DE.B.B1) ? FLAG_C : 0);
    m_DE.B.B1 = (m_DE.B.B1 >> 1) | (BIT7(m_DE.B.B1));
    m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z);
    break;
case 0x2B:
    // SRA E
    m_AF.B.B0 = (BIT1(m_DE.B.B0) ? FLAG_C : 0);
    m_DE.B.B0 = (m_DE.B.B0 >> 1) | (BIT7(m_DE.B.B0));
    m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z);
    break;
case 0x2C:
    // SRA H
    m_AF.B.B0 = (BIT1(m_HL.B.B1) ? FLAG_C : 0);
    m_HL.B.B1 = (m_HL.B.B1 >> 1) | (BIT7(m_HL.B.B1));
    m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z);
    break;
case 0x2D:
    // SRA L
    m_AF.B.B0 = (BIT1(m_HL.B.B0) ? FLAG_C : 0);
    m_HL.B.B0 = (m_HL.B.B0 >> 1) | (BIT7(m_HL.B.B0));
    m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z);
    break;
case 0x2E:
    // SRA (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (BIT1(tempValue) ? FLAG_C : 0);
    tempValue = (tempValue >> 1) | (BIT7(tempValue));
    m_AF.B.B0 = (tempValue ? 0 : FLAG_Z);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x2F:
    // SRA A
    m_AF.B.B0 = (BIT1(m_AF.B.B1) ? FLAG_C : 0);
    m_AF.B.B1 = (m_AF.B.B1 >> 1) | (BIT7(m_AF.B.B1));
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0x30:
    // SWAP B
    m_BC.B.B1 = (m_BC.B.B1 & 0xf0) >> 4 | (m_BC.B.B1 & 0x0f) << 4;
    m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z);
    break;
case 0x31:
    // SWAP C
    m_BC.B.B0 = (m_BC.B.B0 & 0xf0) >> 4 | (m_BC.B.B0 & 0x0f) << 4;
    m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z);
    break;
case 0x32:
    // SWAP D
    m_DE.B.B1 = (m_DE.B.B1 & 0xf0) >> 4 | (m_DE.B.B1 & 0x0f) << 4;
    m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z);
    break;
case 0x33:
    // SWAP E
    m_DE.B.B0 = (m_DE.B.B0 & 0xf0) >> 4 | (m_DE.B.B0 & 0x0f) << 4;
    m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z);
    break;
case 0x34:
    // SWAP H
    m_HL.B.B1 = (m_HL.B.B1 & 0xf0) >> 4 | (m_HL.B.B1 & 0x0f) << 4;
    m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z);
    break;
case 0x35:
    // SWAP L
    m_HL.B.B0 = (m_HL.B.B0 & 0xf0) >> 4 | (m_HL.B.B0 & 0x0f) << 4;
    m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z);
    break;
case 0x36:
    // SWAP (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue = (tempValue & 0xf0) >> 4 | (tempValue & 0x0f) << 4;
    m_AF.B.B0 = (tempValue ? 0 : FLAG_Z);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x37:
    // SWAP A
    m_AF.B.B1 = (m_AF.B.B1 & 0xf0) >> 4 | (m_AF.B.B1 & 0x0f) << 4;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0x38:
    // SRL B
    m_AF.B.B0 = (BIT0(m_BC.B.B1)) ? FLAG_C : 0;
    m_BC.B.B1 >>= 1;
    m_AF.B.B0 = (m_BC.B.B1 ? 0 : FLAG_Z);
    break;
case 0x39:
    // SRL C
    m_AF.B.B0 = (BIT0(m_BC.B.B0)) ? FLAG_C : 0;
    m_BC.B.B0 >>= 1;
    m_AF.B.B0 = (m_BC.B.B0 ? 0 : FLAG_Z);
    break;
case 0x3A:
    // SRL D
    m_AF.B.B0 = (BIT0(m_DE.B.B1)) ? FLAG_C : 0;
    m_DE.B.B1 >>= 1;
    m_AF.B.B0 = (m_DE.B.B1 ? 0 : FLAG_Z);
    break;
case 0x3B:
    // SRL E
    m_AF.B.B0 = (BIT0(m_DE.B.B0)) ? FLAG_C : 0;
    m_DE.B.B0 >>= 1;
    m_AF.B.B0 = (m_DE.B.B0 ? 0 : FLAG_Z);
    break;
case 0x3C:
    // SRL H
    m_AF.B.B0 = (BIT0(m_HL.B.B1)) ? FLAG_C : 0;
    m_HL.B.B1 >>= 1;
    m_AF.B.B0 = (m_HL.B.B1 ? 0 : FLAG_Z);
    break;
case 0x3D:
    // SRL L
    m_AF.B.B0 = (BIT0(m_HL.B.B0)) ? FLAG_C : 0;
    m_HL.B.B0 >>= 1;
    m_AF.B.B0 = (m_HL.B.B0 ? 0 : FLAG_Z);
    break;
case 0x3E:
    // SRL (HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (BIT0(tempValue)) ? FLAG_C : 0;
    tempValue >>= 1;
    m_AF.B.B0 = (tempValue ? 0 : FLAG_Z);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x3F:
    // SRL A
    m_AF.B.B0 = (BIT0(m_AF.B.B1)) ? FLAG_C : 0;
    m_AF.B.B1 >>= 1;
    m_AF.B.B0 = (m_AF.B.B1 ? 0 : FLAG_Z);
    break;
case 0x40:
    // BIT 0,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x41:
    // BIT 0,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x42:
    // BIT 0,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x43:
    // BIT 0,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x44:
    // BIT 0,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x45:
    // BIT 0,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x46:
    // BIT 0,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x47:
    // BIT 0,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 0) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x48:
    // BIT 1,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x49:
    // BIT 1,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x4A:
    // BIT 1,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x4B:
    // BIT 1,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x4C:
    // BIT 1,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x4D:
    // BIT 1,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x4E:
    // BIT 1,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x4F:
    // BIT 1,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 1) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x50:
    // BIT 2,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x51:
    // BIT 2,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x52:
    // BIT 2,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x53:
    // BIT 2,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x54:
    // BIT 2,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x55:
    // BIT 2,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x56:
    // BIT 2,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x57:
    // BIT 2,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 2) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x58:
    // BIT 3,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x59:
    // BIT 3,C
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x5A:
    // BIT 3,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x5B:
    // BIT 3,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x5C:
    // BIT 3,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x5D:
    // BIT 3,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x5E:
    // BIT 3,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x5F:
    // BIT 3,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 3) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x60:
    // BIT 4,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x61:
    // BIT 4,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x62:
    // BIT 4,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x63:
    // BIT 4,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x64:
    // BIT 4,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x65:
    // BIT 4,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x66:
    // BIT 4,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x67:
    // BIT 4,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 4) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x68:
    // BIT 5,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x69:
    // BIT 5,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x6A:
    // BIT 5,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x6B:
    // BIT 5,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x6C:
    // BIT 5,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x6D:
    // BIT 5,L
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x6E:
    // BIT 5,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x6F:
    // BIT 5,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 5) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x70:
    // BIT 6,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x71:
    // BIT 6,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x72:
    // BIT 6,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x73:
    // BIT 6,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x74:
    // BIT 6,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x75:
    // BIT 6,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x76:
    // BIT 6,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x77:
    // BIT 6,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 6) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x78:
    // BIT 7,B
    m_AF.B.B0 = (m_BC.B.B1 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x79:
    // BIT 7,C
    m_AF.B.B0 = (m_BC.B.B0 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x7A:
    // BIT 7,D
    m_AF.B.B0 = (m_DE.B.B1 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x7B:
    // BIT 7,E
    m_AF.B.B0 = (m_DE.B.B0 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x7C:
    // BIT 7,H
    m_AF.B.B0 = (m_HL.B.B1 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x7D:
    // BIT 7,L
    m_AF.B.B0 = (m_HL.B.B0 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x7E:
    // BIT 7,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    m_AF.B.B0 = (tempValue & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x7F:
    // BIT 7,A
    m_AF.B.B0 = (m_AF.B.B1 & (1 << 7) ? 0 : FLAG_Z) |
                FLAG_H | (m_AF.B.B0 & FLAG_C);
    break;
case 0x80:
    // RES 0,B
    m_BC.B.B1 &= ~(1 << 0);
    break;
case 0x81:
    // RES 0,C
    m_BC.B.B0 &= ~(1 << 0);
    break;
case 0x82:
    // RES 0,D
    m_DE.B.B1 &= ~(1 << 0);
    break;
case 0x83:
    // RES 0,E
    m_DE.B.B0 &= ~(1 << 0);
    break;
case 0x84:
    // RES 0,H
    m_HL.B.B1 &= ~(1 << 0);
    break;
case 0x85:
    // RES 0,L
    m_HL.B.B0 &= ~(1 << 0);
    break;
case 0x86:
    // RES 0,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 0);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x87:
    // RES 0,A
    m_AF.B.B1 &= ~(1 << 0);
    break;
case 0x88:
    // RES 1,B
    m_BC.B.B1 &= ~(1 << 1);
    break;
case 0x89:
    // RES 1,C
    m_BC.B.B0 &= ~(1 << 1);
    break;
case 0x8A:
    // RES 1,D
    m_DE.B.B1 &= ~(1 << 1);
    break;
case 0x8B:
    // RES 1,E
    m_DE.B.B0 &= ~(1 << 1);
    break;
case 0x8C:
    // RES 1,H
    m_HL.B.B1 &= ~(1 << 1);
    break;
case 0x8D:
    // RES 1,L
    m_HL.B.B0 &= ~(1 << 1);
    break;
case 0x8E:
    // RES 1,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 1);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x8F:
    // RES 1,A
    m_AF.B.B1 &= ~(1 << 1);
    break;
case 0x90:
    // RES 2,B
    m_BC.B.B1 &= ~(1 << 2);
    break;
case 0x91:
    // RES 2,C
    m_BC.B.B0 &= ~(1 << 2);
    break;
case 0x92:
    // RES 2,D
    m_DE.B.B1 &= ~(1 << 2);
    break;
case 0x93:
    // RES 2,E
    m_DE.B.B0 &= ~(1 << 2);
    break;
case 0x94:
    // RES 2,H
    m_HL.B.B1 &= ~(1 << 2);
    break;
case 0x95:
    // RES 2,L
    m_HL.B.B1 &= ~(1 << 2);
    break;
case 0x96:
    // RES 2,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 2);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x97:
    // RES 2,A
    m_AF.B.B1 &= ~(1 << 2);
    break;
case 0x98:
    // RES 3,B
    m_BC.B.B1 &= ~(1 << 3);
    break;
case 0x99:
    // RES 3,C
    m_BC.B.B0 &= ~(1 << 3);
    break;
case 0x9A:
    // RES 3,D
    m_DE.B.B1 &= ~(1 << 3);
    break;
case 0x9B:
    // RES 3,E
    m_DE.B.B0 &= ~(1 << 3);
    break;
case 0x9C:
    // RES 3,H
    m_HL.B.B1 &= ~(1 << 3);
    break;
case 0x9D:
    // RES 3,L
    m_HL.B.B1 &= ~(1 << 3);
    break;
case 0x9E:
    // RES 3,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 3);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0x9F:
    // RES 3,A
    m_AF.B.B1 &= ~(1 << 3);
    break;
case 0xA0:
    // RES 4,B
    m_BC.B.B1 &= ~(1 << 4);
    break;
case 0xA1:
    // RES 4,C
    m_BC.B.B0 &= ~(1 << 4);
    break;
case 0xA2:
    // RES 4,D
    m_DE.B.B1 &= ~(1 << 4);
    break;
case 0xA3:
    // RES 4,E
    m_DE.B.B0 &= ~(1 << 4);
    break;
case 0xA4:
    // RES 4,H
    m_HL.B.B1 &= ~(1 << 4);
    break;
case 0xA5:
    // RES 4,L
    m_HL.B.B0 &= ~(1 << 4);
    break;
case 0xA6:
    // RES 4,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 4);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xA7:
    // RES 4,A
    m_AF.B.B1 &= ~(1 << 4);
    break;
case 0xA8:
    // RES 5,B
    m_BC.B.B1 &= ~(1 << 5);
    break;
case 0xA9:
    // RES 5,C
    m_BC.B.B0 &= ~(1 << 5);
    break;
case 0xAA:
    // RES 5,D
    m_DE.B.B1 &= ~(1 << 5);
    break;
case 0xAB:
    // RES 5,E
    m_DE.B.B0 &= ~(1 << 5);
    break;
case 0xAC:
    // RES 5,H
    m_BC.B.B1 &= ~(1 << 5);
    break;
case 0xAD:
    // RES 5,L
    m_HL.B.B0 &= ~(1 << 5);
    break;
case 0xAE:
    // RES 5,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 5);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xAF:
    // RES 5,A
    m_AF.B.B1 &= ~(1 << 5);
    break;
case 0xB0:
    // RES 6,B
    m_BC.B.B1 &= ~(1 << 6);
    break;
case 0xB1:
    // RES 6,C
    m_BC.B.B0 &= ~(1 << 6);
    break;
case 0xB2:
    // RES 6,D
    m_DE.B.B1 &= ~(1 << 6);
    break;
case 0xB3:
    // RES 6,E
    m_DE.B.B0 &= ~(1 << 6);
    break;
case 0xB4:
    // RES 6,H
    m_HL.B.B1 &= ~(1 << 6);
    break;
case 0xB5:
    // RES 6,L
    m_HL.B.B0 &= ~(1 << 6);
    break;
case 0xB6:
    // RES 6,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 6);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xB7:
    // RES 6,A
    m_AF.B.B1 &= ~(1 << 6);
    break;
case 0xB8:
    // RES 7,B
    m_BC.B.B1 &= ~(1 << 7);
    break;
case 0xB9:
    // RES 7,C
    m_BC.B.B0 &= ~(1 << 7);
    break;
case 0xBA:
    // RES 7,D
    m_DE.B.B1 &= ~(1 << 7);
    break;
case 0xBB:
    // RES 7,E
    m_DE.B.B0 &= ~(1 << 7);
    break;
case 0xBC:
    // RES 7,H
    m_HL.B.B1 &= ~(1 << 7);
    break;
case 0xBD:
    // RES 7,L
    m_HL.B.B1 &= ~(1 << 7);
    break;
case 0xBE:
    // RES 7,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue &= ~(1 << 7);
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xBF:
    // RES 7,A
    m_AF.B.B1 &= ~(1 << 7);
    break;
case 0xC0:
    // SET 0,B
    m_BC.B.B1 |= 1 << 0;
    break;
case 0xC1:
    // SET 0,C
    m_BC.B.B0 |= 1 << 0;
    break;
case 0xC2:
    // SET 0,D
    m_DE.B.B1 |= 1 << 0;
    break;
case 0xC3:
    // SET 0,E
    m_DE.B.B0 |= 1 << 0;
    break;
case 0xC4:
    // SET 0,H
    m_HL.B.B1 |= 1 << 0;
    break;
case 0xC5:
    // SET 0,L
    m_HL.B.B0 |= 1 << 0;
    break;
case 0xC6:
    // SET 0,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 0;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xC7:
    // SET 0,A
    m_AF.B.B1 |= 1 << 0;
    break;
case 0xC8:
    // SET 1,B
    m_BC.B.B1 |= 1 << 1;
    break;
case 0xC9:
    // SET 1,C
    m_BC.B.B0 |= 1 << 1;
    break;
case 0xCA:
    // SET 1,D
    m_DE.B.B1 |= 1 << 1;
    break;
case 0xCB:
    // SET 1,E
    m_DE.B.B0 |= 1 << 1;
    break;
case 0xCC:
    // SET 1,H
    m_HL.B.B1 |= 1 << 1;
    break;
case 0xCD:
    // SET 1,L
    m_HL.B.B0 |= 1 << 1;
    break;
case 0xCE:
    // SET 1,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 1;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xCF:
    // SET 1,A
    m_AF.B.B1 |= 1 << 1;
    break;
case 0xD0:
    // SET 2,B
    m_BC.B.B1 |= 1 << 2;
    break;
case 0xD1:
    // SET 2,C
    m_BC.B.B0 |= 1 << 2;
    break;
case 0xD2:
    // SET 2,D
    m_DE.B.B1 |= 1 << 2;
    break;
case 0xD3:
    // SET 2,E
    m_DE.B.B0 |= 1 << 2;
    break;
case 0xD4:
    // SET 2,H
    m_HL.B.B1 |= 1 << 2;
    break;
case 0xD5:
    // SET 2,L
    m_HL.B.B0 |= 1 << 2;
    break;
case 0xD6:
    // SET 2,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 2;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xD7:
    // SET 2,A
    m_AF.B.B1 |= 1 << 2;
    break;
case 0xD8:
    // SET 3,B
    m_BC.B.B1 |= 1 << 3;
    break;
case 0xD9:
    // SET 3,C
    m_BC.B.B0 |= 1 << 3;
    break;
case 0xDA:
    // SET 3,D
    m_DE.B.B1 |= 1 << 3;
    break;
case 0xDB:
    // SET 3,E
    m_DE.B.B0 |= 1 << 3;
    break;
case 0xDC:
    // SET 3,H
    m_HL.B.B1 |= 1 << 3;
    break;
case 0xDD:
    // SET 3,L
    m_HL.B.B0 |= 1 << 3;
    break;
case 0xDE:
    // SET 3,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 3;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xDF:
    // SET 3,A
    m_AF.B.B1 |= 1 << 3;
    break;
case 0xE0:
    // SET 4,B
    m_BC.B.B1 |= 1 << 4;
    break;
case 0xE1:
    // SET 4,C
    m_BC.B.B0 |= 1 << 4;
    break;
case 0xE2:
    // SET 4,D
    m_DE.B.B1 |= 1 << 4;
    break;
case 0xE3:
    // SET 4,E
    m_DE.B.B0 |= 1 << 4;
    break;
case 0xE4:
    // SET 4,H
    m_HL.B.B1 |= 1 << 4;
    break;
case 0xE5:
    // SET 4,L
    m_HL.B.B0 |= 1 << 4;
    break;
case 0xE6:
    // SET 4,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 4;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xE7:
    // SET 4,A
    m_AF.B.B1 |= 1 << 4;
    break;
case 0xE8:
    // SET 5,B
    m_BC.B.B1 |= 1 << 5;
    break;
case 0xE9:
    // SET 5,C
    m_BC.B.B0 |= 1 << 5;
    break;
case 0xEA:
    // SET 5,D
    m_DE.B.B1 |= 1 << 5;
    break;
case 0xEB:
    // SET 5,E
    m_DE.B.B0 |= 1 << 5;
    break;
case 0xEC:
    // SET 5,H
    m_HL.B.B1 |= 1 << 5;
    break;
case 0xED:
    // SET 5,L
    m_HL.B.B1 |= 1 << 5;
    break;
case 0xEE:
    // SET 5,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 5;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xEF:
    // SET 5,A
    m_AF.B.B1 |= 1 << 5;
    break;
case 0xF0:
    // SET 6,B
    m_BC.B.B1 |= 1 << 6;
    break;
case 0xF1:
    // SET 6,C
    m_BC.B.B0 |= 1 << 6;
    break;
case 0xF2:
    // SET 6,D
    m_DE.B.B1 |= 1 << 6;
    break;
case 0xF3:
    // SET 6,E
    m_DE.B.B0 |= 1 << 6;
    break;
case 0xF4:
    // SET 6,H
    m_HL.B.B1 |= 1 << 6;
    break;
case 0xF5:
    // SET 6,L
    m_HL.B.B0 |= 1 << 6;
    break;
case 0xF6:
    // SET 6,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 6;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xF7:
    // SET 6,A
    m_AF.B.B1 |= 1 << 6;
    break;
case 0xF8:
    // SET 7,B
    m_BC.B.B1 |= 1 << 7;
    break;
case 0xF9:
    // SET 7,C
    m_BC.B.B0 |= 1 << 7;
    break;
case 0xFA:
    // SET 7,D
    m_DE.B.B1 |= 1 << 7;
    break;
case 0xFB:
    // SET 7,E
    m_DE.B.B1 |= 1 << 7;
    break;
case 0xFC:
    // SET 7,H
    m_HL.B.B1 |= 1 << 7;
    break;
case 0xFD:
    // SET 7,L
    m_HL.B.B0 |= 1 << 7;
    break;
case 0xFE:
    // SET 7,(HL)
    tempValue = m_memory.readMemory(m_HL.W);
    tempValue |= 1 << 7;
    m_memory.writeMemory(m_HL.W, tempValue);
    break;
case 0xFF:
    // SET 7,A
    m_AF.B.B1 |= 1 << 7;
    break;
default:
    // Unimplemented
    break;
}

#endif
