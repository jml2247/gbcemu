/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QElapsedTimer>
#include <QTime>

#include <KXmlGuiWindow>

// Core
#include "GBSystem.h"

class QTimer;

// GUI
class DisplayWidget;

class MainWindow : public KXmlGuiWindow
{
    Q_OBJECT
public:
    MainWindow();

    void loadRom(const QString &filename);

private:
    GBSystem m_gbSystem;
    QTimer *m_timer;
    int m_timeoutCounter;
    QElapsedTimer m_timeCounter;
    bool m_running;
    bool m_paused;

    DisplayWidget *m_displayWidget;

    void setupActions();

private Q_SLOTS:
    void initObject();
    void initGUI();
    void runFrame();
    void pauseEmulation();
    void resumeEmulation();
    void slotOpen();
    void slotQuit();
};

#endif
