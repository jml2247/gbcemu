/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *   Copyright (C) 2009-2010  VBA development team                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "DisplayWidget.h"

#include <QResizeEvent>
#include <QPainter>

#include "Globals.h"

DisplayWidget::DisplayWidget(QWidget *parent)
    : QWidget(parent)
    , m_placement(QRectF(0, 0, GB_SCREEN_W, GB_SCREEN_H))
    , m_smooth(true)
{
    setMinimumSize(GB_SCREEN_W, GB_SCREEN_H);
    setFocusPolicy(Qt::StrongFocus);
    setAttribute(Qt::WA_OpaquePaintEvent);

    m_pixels = QImage(GB_SCREEN_W, GB_SCREEN_H, QImage::Format_RGB444);
    m_pixels.fill(0x0000);
}

void DisplayWidget::displayFrame(const std::vector<uint16_t> &frame)
{
    auto dest = reinterpret_cast<uint16_t *>(m_pixels.bits());

    for (const auto &pixel : frame) {
        *(dest++) = pixel;
    }

    this->repaint();
}

void DisplayWidget::resizeEvent( QResizeEvent *event )
{
    const int widgetWidth  = event->size().width();
    const int widgetHeight = event->size().height();
    const int scaleX = widgetWidth / GB_SCREEN_W;
    const int scaleY = widgetHeight / GB_SCREEN_H;
    const int scaleMin = std::min(scaleX, scaleY);
    const int scaledWidth  = scaleMin * GB_SCREEN_W;
    const int scaledHeight = scaleMin * GB_SCREEN_H;
    const int left = (widgetWidth  - scaledWidth) / 2;
    const int top  = (widgetHeight - scaledHeight) / 2;

    m_placement.setTopLeft(QPointF(left, top));
    m_placement.setSize(QSizeF(scaledWidth, scaledHeight));
}

void DisplayWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter p(this);
    p.setRenderHint(QPainter::SmoothPixmapTransform, m_smooth);
    p.drawImage(m_placement, m_pixels);
}
