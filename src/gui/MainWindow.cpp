/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "MainWindow.h"

// Qt includes
#include <QAction>
#include <QApplication>
#include <QDebug>
#include <QStatusBar>
#include <QFileDialog>
#include <QtCore/QTimer>
#include <QVBoxLayout>

// KDE includes
#include <KLocalizedString>
#include <KActionCollection>
#include <KStandardAction>

// Own includes
#include "DisplayWidget.h"

MainWindow::MainWindow()
    : m_timer(nullptr)
    , m_timeoutCounter(0)
    , m_running(false)
    , m_paused(false)
    , m_displayWidget(nullptr)
{
    initObject();
    initGUI();
}

void MainWindow::initObject()
{
    m_timer = new QTimer(this);
    m_timer->setInterval(5);
    connect(m_timer, &QTimer::timeout, this, &MainWindow::runFrame);
}

void MainWindow::initGUI()
{
    auto mainWidget = new QWidget(this);
    auto mainLayout = new QVBoxLayout(mainWidget);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainWidget->setLayout(mainLayout);

    m_displayWidget = new DisplayWidget(mainWidget);

    mainLayout->addWidget(m_displayWidget);

    setCentralWidget(mainWidget);

    setupActions();
    statusBar()->show();
    setupGUI();
}

void MainWindow::setupActions()
{
    KStandardAction::open(this, SLOT(slotOpen()), actionCollection());
    KStandardAction::quit(qApp, &QApplication::closeAllWindows, actionCollection());
}

void MainWindow::runFrame()
{
    const int nextTimeout = (1000 * m_timeoutCounter) / 60;
    const int timePassed = m_timeCounter.elapsed();

    if (timePassed >= nextTimeout) {
        m_gbSystem.updateJoypad(0xFF);
        m_gbSystem.runFrame();
        m_displayWidget->displayFrame(m_gbSystem.framebuffer());
        m_timeoutCounter++;

        if((m_timeoutCounter % 60) == 0) {
            const double avgFps = m_timeoutCounter / (timePassed / 1000.0f);
            statusBar()->showMessage(i18n("Timer avg fps: ") + QString::number(avgFps));
        }
    }
}

void MainWindow::pauseEmulation()
{
    if (m_timer->isActive()) {
        m_paused = true;
        m_timer->stop();
    }
}

void MainWindow::resumeEmulation()
{
    if (!m_timer->isActive()) {
        m_paused = false;
        m_timeCounter.start();
        m_timer->start();
    }
}

void MainWindow::slotOpen()
{
    const bool wasPaused = m_paused;
    pauseEmulation();
    QString filename = QFileDialog::getOpenFileName(this, i18nc("@title:window", "Open ROM"));

    if (filename.isEmpty()) {
        if (!wasPaused && m_running) {
            resumeEmulation();
        }
        return;
    }

    m_paused = wasPaused;
    loadRom(filename);
}

void MainWindow::loadRom(const QString &filename)
{
    if (!m_gbSystem.loadCartridge(filename.toStdString())) {
        // TODO: Say something
        qWarning() << "Failed to load cartridge";
        return;
    }

    if (!m_paused) {
        m_running = true;
        m_timeCounter.restart();
        m_timeoutCounter = 0;
        resumeEmulation();
    }
}

void MainWindow::slotQuit()
{
    pauseEmulation();
    QApplication::instance()->quit();
}

