/***************************************************************************
 *   Copyright (C) 2011 by Jonathan Thomas <echidnaman@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA            *
 ***************************************************************************/

#include "MainWindow.h"

#include <KAboutData>
#include <KLocalizedString>
#include <QApplication>
#include <QCommandLineParser>
#include <QUrl>

constexpr auto description = "A Gameboy emulator";

constexpr auto version = "0.1";

int main(int argc, char **argv)
{
    QApplication app{argc, argv};
    KAboutData about("kgb", i18n("KGB"), version, i18n(description),
                     KAboutLicense::GPL, i18n("(C) 2011-2017 Jonathan Thomas"), QString(), QString());
    about.addAuthor(i18n("Jonathan Thomas"), QString(), "echidnaman@kubuntu.org" );
    KAboutData::setApplicationData(about);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("+[URL]", "ROM to open");
    about.setupCommandLine(&parser);

    parser.process(app);

    QStringList args = parser.positionalArguments();

    auto mainWindow = new MainWindow();
    mainWindow->show();

    if (args.count() > 0) {
        mainWindow->loadRom(args.at(0));
    }

    return app.exec();
}
